def enum(**enums):
    return type('Enum', (), enums)


Anonymity = enum(
    ALL="",
    ELITE=1,
    ANONYM=2,
    TRANSPARENT=3
)

Availability = enum(
    ALL="",
    BAD=20,
    OKAY=50,
    GOOD=70,
    VERY_GOOD=80,
    EXCELLENT=90
)

ResponseTime = enum(
    ALL="",
    BAD=10,
    OKAY=7,
    GOOD=5,
    VERY_GOOD=4,
    EXCELLENT=2
)

Country = enum(
    ALL="",
    AFGHANISTAN="AF",
    ALBANIA="AL",
    ARMENIA="AM",
    ANGOLA="AO",
    ARGENTINA="AR",
    AUSTRIA="AT",
    AUSTRALIA="AU",
    AZERBAIJAN="AZ",
    BANGLADESH="BD",
    BELGIUM="BE",
    BULGARIA="BG",
    BOLIVIA="BO",
    BRAZIL="BR",
    BOTSWANA="BW",
    BELARUS="BY",
    CANADA="CA",
    SWITZERLAND="CH",
    CHILE="CL",
    CAMEROON="CM",
    CHINA="CN",
    COLOMBIA="CO",
    CYPRUS="CY",
    GERMANY="EN",
    DENMARK="DK",
    ALGERIA="DZ",
    ECUADOR="EC",
    ESTONIA="EE",
    EGYPT="EC",
    SPAIN="ES",
    FINLAND="FI",
    FRANCE="FR",
    GEORGIA="GE",
    GHANA="GH",
    GREECE="GR",
    GUATEMALA="GT",
    CROATIA="HR",
    HUNGARY="HU",
    INDONESIA="ID",
    ISRAEL="IL",
    INDIA="IN",
    IRAQ="IQ",
    IRAN="IR",
    ITALY="IT",
    JAPAN="JP",
    KENYA="KE",
    KYRGYZSTAN="KG",
    CAMBODIA="KH",
    KOREA="KR",
    KAZAKHSTAN="KZ",
    LESOTHO="LS",
    LUXEMBOURG="LU",
    LATVIA="LV",
    MOROCCO="MA",
    MOLDOVA="MD",
    MADAGASCAR="MG",
    MYANMAR="MM",
    MONGOLIA="MN",
    MALDIVES="MV",
    MEXICO="MX",
    MALAYSIA="MY",
    MOZAMBIQUE="MZ",
    NIGERIA="NG",
    NETHERLANDS="NL",
    NORWAY="NO",
    NEPAL="NP",
    PANAMA="PA",
    PERU="PE",
    PHILIPPINES="PH",
    PAKISTAN="PK",
    POLAND="PL",
    PORTUGAL="PT",
    PARAGUAY="PY",
    ROMANIA="RO",
    SERBIA="RS",
    RUSSIA="RU",
    SWEDEN="SE",
    SINGAPORE="SG",
    SLOVAKIA="SK",
    SENEGAL="SN",
    THAILAND="TH",
    TUNISIA="TN",
    TURKEY="TR",
    TAIWAN="TW",
    TANZANIA="TZ",
    UKRAINE="UA",
    VENEZUELA="VE",
    VIETNAM="VN",
    AFRICA="ZA",
    ZAMBIA="ZM",
    ZIMBABWE="ZW",
)
