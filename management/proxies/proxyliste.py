#!/usr/local/bin/python
# coding: utf-8
import re

from bs4 import BeautifulSoup

from management import session
from management.proxies.settings import *

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


class ProxyListe(object):
    """
    proxy search for proxy-listen.de
    """

    url = "http://www.proxy-listen.de/Proxy/Proxyliste.html"

    def __init__(self):
        """
        initializing function

        :return:
        """
        self.session = session.Session().get_session("phantomJS")
        self.session.open(self.url)

    def multiselect_set_selections(self, element_id, labels):
        """
        select option based on id and label

        :param element_id:
        :param labels:
        :return:
        """
        el = self.session.find_element_by_id(element_id)
        for option in el.find_elements_by_tag_name('option'):
            if option.get_attribute("value") in labels:
                option.click()

    def set_filter(self, availability=Availability.GOOD, response=ResponseTime.GOOD, country=Country.ALL,
                   anonymity=Anonymity.ALL):
        """
        filter the results with self defined presets

        :param availability:
        :param response:
        :param country:
        :param anonymity:
        :return:
        """
        self.multiselect_set_selections("anonymity", (str(anonymity)))
        self.multiselect_set_selections("ping", (str(response)))
        self.multiselect_set_selections("downtime", (str(availability)))
        self.multiselect_set_selections("country", (str(country)))
        self.multiselect_set_selections("proxies", ("300",))

    def search(self):
        """
        search the resulting proxies

        :return:
        """
        self.session.find_element_by_xpath('//input[@name = "submit"]').click()
        content = self.session.read()
        soup = BeautifulSoup(content, "lxml")
        proxies = soup.find_all("tr", {"class": "proxyListEven"}) + soup.find_all("tr", {"class": "proxyListOdd"})
        untested_proxies = {}
        for proxy in proxies:
            try:
                ip, port, gateway, level, ping, country, uptime, check, _, _ = proxy.find_all("td")
            except ValueError:
                continue
            ip = ip.text
            if ip in untested_proxies:
                continue
            port = port.text
            level = level.text
            ping = re.findall('([\d.]+) ', ping.text)[0]
            country_check = country.find("img")
            if country_check:
                country = country_check["title"]
            else:
                country = country.text
            uptime = uptime.text.split("%")[0].strip()
            if check:
                check = check.text.strip()
            untested_proxies[ip] = {
                "port": port,
                "level": level,
                "ping": ping,
                "country": country,
                "uptime": uptime,
                "check": check
            }
        return untested_proxies


instance = ProxyListe

if __name__ == '__main__':
    p = ProxyListe()
    p.set_filter()
    p.search()
