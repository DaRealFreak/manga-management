#!/usr/local/bin/python
# coding: utf-8

import datetime
import logging
import os
import sqlite3
import threading
import time

import xml_parser

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


# noinspection PyArgumentList,PyShadowingNames,PyShadowingBuiltins
class DatabaseIO(object):
    """
    Singleton class via override
    used for storing the large information stream
    of informations to the updated mangas
    """

    instance = None
    conn = None
    cursor = None

    lock = threading.Lock()
    logger = logging.getLogger()

    def __init__(self):
        """
        initializing function

        :return:
        """
        parser = xml_parser.XMLParser()
        self.db_path = parser.get_database_path()
        if not self.cursor:
            self.open_database()

    def __del__(self):
        if self.conn or self.cursor:
            self.commit()
            self.close_database()

    def __new__(cls, *args, **kwargs):
        """
        create super instance from the current object

        :param cls:
        :param args:
        :param kwargs:
        :return:
        """
        if not cls.instance:
            cls.instance = super(DatabaseIO, cls).__new__(cls, *args, **kwargs)
        return cls.instance

    def create_database(self, file):
        """
        create a database if it's non existent

        :param file:
        :return:
        """
        try:
            sql_update_log = """
              CREATE TABLE IF NOT EXISTS UpdateLog (
                    idx INTEGER PRIMARY KEY,
                    manga TEXT,
                    chapter TEXT,
                    update_time INTEGER
                 );
            """
            sql_download_queue = """
              CREATE TABLE IF NOT EXISTS DownloadQueue (
                    idx INTEGER PRIMARY KEY,
                    path TEXT,
                    url TEXT,
                    chapter_index INTEGER,
                    chapter_count INTEGER,
                    chapter_title TEXT,
                    manga_title TEXT,
                    UNIQUE(path, url)
                );
            """
            sql_manga_database = """
                CREATE TABLE IF NOT EXISTS Manga (
                    idx INTEGER PRIMARY KEY,
                    title TEXT COLLATE NOCASE,
                    chapter TEXT,
                    language TEXT,
                    url TEXT,
                    module TEXT,
                    active INTEGER,
                    UNIQUE(title, url)
                );
            """
            self.cursor.execute(sql_update_log)
            self.cursor.execute(sql_download_queue)
            self.cursor.execute(sql_manga_database)
        except Exception:
            raise IOError(u"Couldn't connect to database file: {0:s}".format(file))

    def open_database(self):
        """
        open the database or create it if it's non existent

        :return:
        """
        if self.conn or self.cursor:
            raise AttributeError("Connection is already established")
        if self.db_path == "":
            raise IOError("File path can't be an empty string.")
        if self.db_path.startswith(".."):
            self.db_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), self.db_path)

        create = not os.path.exists(self.db_path) or not os.path.isfile(self.db_path)
        self.conn = sqlite3.connect(self.db_path, check_same_thread=False)
        self.cursor = self.conn.cursor()

        if create:
            self.create_database(self.db_path)

    def close_database(self):
        """
        close the database and delete the variables

        :return:
        """
        self.commit()
        self.conn.close()
        self.conn = None
        self.cursor = None

    def update_database_format(self, table, struct, rows_new, rows_old):
        """
        script for updating structures in the database to take over the old database

        :param table:
        :param struct:
        :param rows_new:
        :param rows_old:
        :return:
        """
        update_queries = [
            'CREATE TABLE {0:s}_Copy({1:s});'.format(table, struct),
            'INSERT INTO {0:s}_Copy ({1:s}) SELECT {2:s} FROM {0:s};'.format(table, ','.join(rows_new),
                                                                             ','.join(rows_old)),
            'DROP TABLE {0:s};'.format(table),
            'ALTER TABLE {0:s}_Copy RENAME TO {0:s};'.format(table),
        ]
        for query in update_queries:
            self.execute_query(query)

    def dump_database(self, dump_file):
        """
        debug function to dump the database

        :param dump_file:
        :return:
        """
        with open(dump_file, 'w+') as f:
            for line in self.conn.iterdump():
                f.write(u'{0:s}\n'.format(line))

    def commit(self):
        """
        commiting manually for saving performance

        :return:
        """
        try:
            self.conn.commit()
        except sqlite3.OperationalError:
            pass

    def execute_query(self, query, *args, **kwargs):
        """
        execute the query in the database with synchronized blocks

        :param kwargs:
        :param args:
        :param query:
        :return:
        """
        result = kwargs.pop("result", False)
        #        if args:
        #            query = self.escape_query(query, args)
        try:
            self.lock.acquire(True)
            if not args:
                self.cursor.execute(query)
            else:
                self.cursor.execute(query, args)
            if result:
                return self.cursor.fetchall()
        except sqlite3.ProgrammingError as err:
            self.logger.warn("An error occured: {0:s}".format(err))
        except sqlite3.OperationalError:
            self.execute_query(query, args, kwargs)
        finally:
            self.lock.release()

    def add_download_queue(self, dl_object):
        """
        add a download link including path to the database for further usage

        :param dl_object:
        :return:
        """
        path, url, chapter_index, chapter_count, chapter_title, manga_title = dl_object
        self.execute_query(
            "INSERT OR IGNORE INTO DownloadQueue(path, url, chapter_index, chapter_count, chapter_title,"
            "manga_title) VALUES (?, ?, ?, ?, ?, ?)", path, url, chapter_index, chapter_count, chapter_title,
            manga_title)

    def get_latest_chapter_in_queue(self, title):
        """
        if a chapter is completely saved in the download queue and the process is killed
        we can reconstruct the currently parsed chapter based on the database entries to avoid
        unneccessary traffic

        :param title:
        :return:
        """
        self.commit()
        return self.execute_query("SELECT * FROM DownloadQueue WHERE chapter_index = chapter_count AND "
                                  "manga_title=? ORDER BY idx LIMIT 1", title, result=True)

    def del_download(self, path, url):
        """
        delete an object identified by the uid from the download queue

        :param path:
        :param url:
        :return:
        """
        """
        not deleting through identifier on purpose
        update only executed if all downloads are finished to keep track if the database encounters an error
        """
        self.execute_query(u"DELETE FROM DownloadQueue WHERE path=? AND url=?", path, url, result=False)

    def get_download_queue(self):
        """
        return the download queue

        :return:
        """
        self.commit()
        return self.execute_query('SELECT * FROM DownloadQueue ORDER BY idx', result=True)

    def add_update(self, manga, chapter):
        """
        add an update to the current log

        :param manga:
        :param chapter:
        :return:
        """
        self.execute_query("INSERT INTO UpdateLog(manga, chapter, update_time) VALUES (?, ?, ?)",
                           manga, chapter, time.time())

    def get_updatelog(self, limit=20):
        """
        return the update log

        :param limit:
        :return:
        """
        self.commit()
        log = self.execute_query("SELECT * FROM UpdateLog ORDER BY update_time DESC LIMIT ?", limit,
                                 result=True)
        log.reverse()
        return log

    def add_manga(self, title, chapter, language, url, module, active):
        """
        add a new manga to the database

        :param title:
        :param chapter:
        :param language:
        :param url:
        :param module:
        :param active:
        :return:
        """
        self.execute_query("INSERT OR IGNORE INTO Manga(title, chapter, language, url, module, active)" +
                           " VALUES (?, ?, ?, ?, ?, ?)", title, chapter, language, url, module, active)

    def delete_manga(self, title):
        """
        delete a manga based on the title from the database

        :param title:
        :return:
        """
        self.execute_query(u"DELETE FROM Manga WHERE title=?", title)

    def get_mangas(self):
        """
        return the mangas from the database

        :return:
        """
        self.commit()
        return self.execute_query('SELECT * FROM Manga ORDER BY idx', result=True)

    def get_manga(self, title):
        """
        return the specific details of a manga from the database

        :param title:
        :return:
        """
        self.commit()
        result = self.execute_query(u'SELECT * FROM Manga WHERE title=? LIMIT 1', title, result=True)
        column_names = self.execute_query('PRAGMA table_info(Manga);', result=True)
        if column_names and result:
            manga_result = {}
            for column in column_names:
                index = column_names.index(column)
                manga_result[column[1]] = result[0][index]
            return manga_result
        return None

    def update_manga(self, title, key, value):
        """
        update the manga based on the title in the database

        :param title: string
        :param key: string
        :param value: string|int
        :return:
        """
        manga = self.get_manga(title)
        self.execute_query(u"UPDATE Manga SET '{0:s}'=? WHERE idx=?".format(key), value, manga['idx'])


if __name__ == '__main__':
    # needed logger for main module testing
    logger = logging.getLogger()
    log_formatter = logging.Formatter("%(asctime)s [%(threadName)s] [%(levelname)-3.5s]  %(message)s")
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    logger.addHandler(console_handler)

    dbIO = DatabaseIO()

    # dbIO.add_update("Deadman Wonderland", "Chapter 73")
    dbIO.dump_database("dump.sql")
    res = dbIO.get_updatelog()
    for row in res:
        print("%s %s: %s" %
              (row[1], row[2], datetime.datetime.fromtimestamp(int(round(row[3]))).strftime('%Y-%m-%d %H:%M:%S')))

    dbIO.add_download_queue(["Mirai Nikki 1/page_001.jpg",
                             "http://www.google.de", 1, 3, "Mirai Nikki 1", "Mirai Nikki"])
    dbIO.add_download_queue(["Mirai Nikki 1/page_002.jpg",
                             "http://www.google.de", 2, 3, "Mirai Nikki 1", "Mirai Nikki"])
    dbIO.add_download_queue(["Mirai Nikki 1/page_003.jpg",
                             "http://www.google.de", 3, 3, "Mirai Nikki 1", "Mirai Nikki"])
    res = dbIO.get_download_queue()
    for row in res:
        idx, path, url, index, count, title, manga = row
        dbIO.del_download(path, url)
        print path, url

    # dbIO.add_manga("title", "chapter", "language", "url", "module", 1)
    print dbIO.get_mangas()
    print dbIO.get_manga('Ore ga Ojou-sama Gakkou ni "Shomin Sample" Toshite Rachirareta Ken')
    # dbIO.update_manga("title", "chapter", "chapter")

    db_update = False
    if db_update:
        struct = """
            idx INTEGER PRIMARY KEY,
            title TEXT COLLATE NOCASE,
            chapter TEXT,
            language TEXT,
            url TEXT,
            module TEXT,
            active INTEGER,
            UNIQUE(title, url)"""
        row_old = ('idx', 'title', 'chapter', 'language', 'url', 'module', 'active')
        row_new = row_old
        dbIO.update_database_format("Manga", struct, row_new, row_old)
