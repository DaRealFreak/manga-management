#!/usr/local/bin/python
# coding: utf-8

import locale
import os
import re

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


class LocalizationException(Exception):
    pass


# noinspection PyArgumentList
class Singleton(type):
    """
    singleton class used for meta class usage
    """

    def __init__(cls, name, bases, dictionary):
        """
        at initializing the class call for the super class to get the instance

        :param cls:
        :param name:
        :param bases:
        :param dictionary:
        :return:
        """
        super(Singleton, cls).__init__(name, bases, dictionary)
        cls.instance = None

    def __call__(cls, *args, **kw):
        """
        create instance if not existant, else return the existant instance

        :param cls:
        :param args:
        :param kw:
        :return:
        """
        if cls.instance is None:
            cls.instance = super(Singleton, cls).__call__(*args, **kw)
        return cls.instance


class Locale(dict):
    """
    small localization dictionary to translate on runtime
    """
    __metaclass__ = Singleton

    def __init__(self, **kwargs):
        """
            initializing function

            :return:
            """
        super(Locale, self).__init__(**kwargs)
        self.lan, self.encoding = locale.getdefaultlocale()
        self.path = None
        self.language_file = None

    def __getattr__(self, key):
        """
        dynamic getter for loaded variables

        :param key:
        :return:
        """
        if key in self:
            return self[key]
        else:
            raise KeyError('Key "%s" is not localized.' % key)

    def set_locale(self, lan):
        """
        manually change the language

        :param lan:
        :return:
        """
        self.lan = lan
        if self.path and self.language_file:
            self.load_locale(self.path, self.language_file)

    def load_locale(self, path, language_file):
        """
        check the locale path and files

        :param path:
        :param language_file:
        :return:
        """
        if language_file == "":
            raise LocalizationException("No localization file specified.")
        if os.path.exists(path):
            self.path = path
            self.language_file = language_file
            spec_language_file = "{0:s}.{1:s}".format(self.lan, language_file)
            specific = os.path.join(path, spec_language_file)
            if os.path.exists(specific) and os.path.isfile(specific):
                self.load(specific)
            else:
                default = os.path.join(path, language_file)
                if os.path.exists(default) and os.path.isfile(default):
                    self.load(default)
                else:
                    raise LocalizationException("Could not find any localization file.")
        else:
            raise LocalizationException("Could not find any localization file.")

    def load(self, path):
        """
        load the translated entries from the localization file

        :param path:
        :return:
        """
        entries = open(path).readlines()
        for entry in entries:
            if re.match("[\w]+=.*", entry):
                var, val = entry.split("=", 1)
                self[var] = val


if __name__ == '__main__':
    l = Locale()
    l.load_locale("bin/language/", "locale.txt")
    print l.keys()
    print l[l.keys()[0]]
    try:
        l.test
    except KeyError:
        print 'key "test" not found'
