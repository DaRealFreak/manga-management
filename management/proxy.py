#!/usr/local/bin/python
# coding: utf-8

import Queue
import fnmatch
import httplib
import imp
import logging
import os
import socket
import sys
import threading
import time
import urllib2

import session
from proxies.settings import *

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


class FilterDict(dict):
    """
    extend the dictionary for our own functions
    """

    # noinspection PyMissingConstructor
    def __init__(self, input_dict):
        """
        initializing function

        :param input_dict:
        :return:
        """
        for key, value in input_dict.iteritems():
            self[key] = value

    def filter(self, criteria):
        """
        filter function, critera has to be a function instance or lambda
        removes keys not fitting the criteria from the dict

        :param criteria:
        """
        for key, value in self.items():
            if not bool(criteria(value)):
                self.pop(key)


class BlackList(list):
    """
    simple list object with the option to filter for dead/expired proxies
    """

    def filter(self, proxy_dict):
        """
        filter the dictionary for proxies which aren't listed in the blacklist

        :param proxy_dict:
        :return:
        """
        return {ip: v for ip, v in proxy_dict.items() if ip not in self}


class ValidatingQueue(threading.Thread):
    """
    validating queue for multi threaded validation
    """

    def __init__(self, queue, validated_proxies, timeout, length, blacklist):
        """
        initializing function

        :param queue:
        :param validated_proxies:
        :param timeout:
        :param blacklist:
        :return:
        """
        threading.Thread.__init__(self)
        self.length = length
        self.queue = queue
        self.HTTP_TIMEOUT = timeout
        self.validated_proxies = validated_proxies
        self.blacklist = blacklist

    def run(self):
        """
        main function of our worker thread

        :return:
        """
        while True:
            ip, proxy, position = self.queue.get()
            if logging.getLogger().isEnabledFor(logging.DEBUG):
                sys.stdout.write('\r' * 50)
                sys.stdout.write('validated: {0:.2f}%'.format(round(100 * float(position) / float(self.length), 2)))
                if int(float(position) / float(self.length)):
                    sys.stdout.write("\n")
                sys.stdout.flush()
            try:
                proxy_handler = urllib2.ProxyHandler({'http': "{0:s}:{1:s}".format(ip, proxy["port"])})
                opener = urllib2.build_opener(proxy_handler)
                opener.addheaders = [('User-agent', session.Session.get_random_useragent())]
                urllib2.install_opener(opener)
                req = urllib2.Request("http://www.google.com")
                sock = urllib2.urlopen(req, timeout=self.HTTP_TIMEOUT)
                rs = sock.read(1000)
                if '<title>Google</title>' in rs:
                    self.validated_proxies.setdefault(ip, proxy)
            except (urllib2.URLError, urllib2.HTTPError, socket.timeout, IOError, httplib.HTTPException, socket.error):
                self.blacklist.append(ip)

            # signals to queue job is done
            self.queue.task_done()


class ProxyManager(object):
    """
    a proxy manager, for managing all the proxy sites and the validation
    of the found proxies
    """

    MAX_THREADS = 4
    TIMEOUT = 4

    modules = dict()
    proxies = dict()
    next_loading_timestamp = 0
    path = os.path.join(os.path.dirname(__file__), "proxies")

    def __init__(self):
        """
        initializing function

        :return:
        """
        self.blacklist = BlackList()
        self.logger = logging.getLogger()

        self.load_modules()
        if self.next_loading_timestamp < time.time():
            self.fetch_proxies()

    def load_modules(self):
        """
        load all modules dynamically

        :return:
        """
        if self.path not in sys.path:
            sys.path.insert(0, self.path)

        for module in fnmatch.filter(os.listdir(self.path), '*.py'):
            if module in ("__init__.py", "settings.py"):
                continue
            session_name = module.rsplit('.', 1)[0]
            m = imp.load_source(session_name, os.path.join(self.path, module))
            self.modules.setdefault(session_name, m.instance)

    def get_proxy(self, uptime=Availability.GOOD, connection=ResponseTime.GOOD, level=Anonymity.ALL):
        """
        filter proxy based on parameters

        :param uptime:
        :param connection:
        :param level:
        :return:
        """
        if len(self.proxies) == 0 or self.next_loading_timestamp < time.time():
            self.fetch_proxies()
        self.proxies.filter(lambda x: True if level == "" else int(x["level"]) >= level)
        self.proxies.filter(lambda x: True if connection == "" else float(x["ping"]) <= connection)
        self.proxies.filter(lambda x: True if uptime == "" else int(x["uptime"]) > uptime)
        if len(self.proxies) == 0:
            raise AttributeError("No proxy fits your description")

        return self.proxies.popitem()

    def validate_proxies(self, proxies):
        """
        validate proxies in a threaded queue

        :param proxies:
        :return:
        """
        self.proxies = FilterDict({})
        queue = Queue.Queue()
        for _ in range(self.MAX_THREADS):
            # using mutable objects for appending to the variable
            t = ValidatingQueue(queue, self.proxies, self.TIMEOUT, len(proxies), self.blacklist)
            t.setDaemon(True)
            t.start()

        self.logger.info("Validating proxies, this could take a while...")
        pos = 1
        for proxy in proxies:
            queue.put((proxy, proxies[proxy], pos))
            pos += 1

        # wait on the queue until everything has been processed before we set the loaded timestamp
        queue.join()
        self.next_loading_timestamp = time.time() + 300
        self.logger.debug("Validating complete, found {0:d} working proxies".format(len(self.proxies)))

    def fetch_proxies(self):
        """
        fetch proxies from the modules

        :return:
        """
        untested_proxies = {}
        self.logger.info("Fetching proxies...")
        for module in self.modules:
            module_object = self.modules[module]()
            module_object.set_filter()
            untested_proxies.update(module_object.search())
        untested_proxies = self.blacklist.filter(untested_proxies)
        self.validate_proxies(untested_proxies)


if __name__ == '__main__':
    p = ProxyManager()
    for i in xrange(15):
        print 'proxy #{0:d}: "{1:s}"'.format(i, p.get_proxy())
