#!/usr/local/bin/python
# coding: utf-8

import inspect
import threading
import time

import databaseIO
import download
import modules
import threads

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


class Updater(object):
    """
    one of the main classes, checking for updates for the database entries
    """

    event = threading.Event()

    def __init__(self):
        """
        initializing function

        :return:
        """
        self.threading = threads.ThreadingManagement()
        self.module_manager = modules.ModuleManager()
        self.dbIO = databaseIO.DatabaseIO()
        # new thread always returns run
        if inspect.stack()[1][3] == 'run':
            self.update()

    def update(self):
        """
        iterating through our saved entries checking for updates

        :return:
        """
        function_caller = inspect.stack()[1][3]
        while True:
            if not self.event.is_set():
                self.event.set()

                for manga_v in self.dbIO.get_mangas():
                    manga = self.dbIO.get_manga(manga_v[1])
                    if not manga["active"]:
                        continue

                    session_module = manga["module"]
                    session_url = manga["url"]
                    session_chapter = self.dbIO.get_latest_chapter_in_queue(manga_v[1])
                    if session_chapter:
                        idx, page, url, index, count, session_chapter, manga = session_chapter[0]
                    else:
                        session_chapter = manga["chapter"]

                    module = self.module_manager.get_module(session_module)
                    if module.get_latest_chapter(session_url)[1] != session_chapter:
                        self.threading.run_once(download.Downloader().check_download_queue, delay=5.0)
                        module.fetch_new_chapters(session_url, manga_v[1], session_chapter)

                self.event.clear()

            if function_caller != '__init__':
                break

            time.sleep(60)


if __name__ == '__main__':
    u = Updater()
    u.update()
