#!/usr/local/bin/python
# coding: utf-8

import fnmatch
import os
import sys

import download
import session

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


class ModuleManager(object):
    """
    small manager for the modules
    """

    modules = dict()
    path = os.path.join(os.path.dirname(__file__), "manga")

    def __init__(self):
        """
        initializing function

        :return:
        """
        self.db_handler = download.DownloadDatabaseHandler()
        self.session_manager = session.Session()
        self.load_modules()

    def load_modules(self):
        """
        load all modules dynamically

        :return:
        """
        if self.path not in sys.path:
            sys.path.insert(0, self.path)

        modules = fnmatch.filter(os.listdir(self.path), '*.py')
        if '__init__.py' in modules:
            modules.remove('__init__.py')

        for module in modules:
            m = __import__(u'{0:s}'.format(module.lower().rsplit('.', 1)[0]))
            if 'setup' in dir(m):
                self.modules[module.lower().rsplit('.', 1)[0]] = m.setup

    def get_module_session(self, module):
        """
        return a loaded module

        :param module:
        :return:
        """
        module = module.lower()
        if module in self.modules:
            _, session_type = self.modules[module]
            session_object = self.session_manager.get_session(session_type)
            return session_object
        else:
            err = "'{0:s}' object has no module '{1:s}'".format(self.__class__.__name__, module)
            raise AttributeError(err)

    def get_module(self, module):
        """
        return a loaded module

        :param module:
        :return:
        """
        module = module.lower()
        if module in self.modules:
            instance, session_type = self.modules[module]
            module_object = instance()
            session_object = self.session_manager.get_session(session_type)
            module_object.set_session(session_object)
            module_object.set_update_function(self.db_handler.add_download)
            return module_object
        else:
            err = "'{0:s}' object has no module '{1:s}'".format(self.__class__.__name__, module)
            raise AttributeError(err)

    def get_all_modules(self):
        """
        return all modules

        :return:
        """
        return {key: self.get_module(key) for key in self.modules.keys()}


if __name__ == '__main__':
    manager = ModuleManager()
    print manager.get_module('mangafox')
    print manager.get_all_modules()
