#!/usr/local/bin/python
# coding: utf-8

import logging

import databaseIO
import download
import interface
import localization
import threads
import title_search
import update
import xml_parser

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


class MangaManagement(object):
    """
    main class of the Management
    """

    def __init__(self):
        """
        init function
        """
        # threading control
        self.define_logger()
        self.threading = threads.ThreadingManagement()
        self.search_manager = title_search.Search()
        self.dbIO = databaseIO.DatabaseIO()
        self.xml_parser = xml_parser.XMLParser()
        self.locale = localization.Locale()
        path = self.xml_parser.get_localization_key("path")
        locale_file = self.xml_parser.get_localization_key("file")
        locale = localization.Locale()
        locale.load_locale(path, locale_file)
        self.threading.add_thread("UpdateThread", update.Updater)
        self.threading.add_thread("DownloadThread", download.Downloader)
        self.threading.start_control_thread()

        # user interface
        self.gui = interface.user_interface()
        if self.xml_parser.get_config("systemtray"):
            self.systemtray = interface.SystemTray()
            self.create_systemtray_menu()
        else:
            self.systemtray = None
            # self.main()

    def exit(self):
        """
        kill threads and leave

        :return:
        """
        self.threading.exit()

    def iterate_test_mangas(self):
        """
        test function for iterating through mangas defined in a test file

        :return:
        """
        mangas = open('../tests/TestMangaList.txt', 'r').readlines()[1:]
        for manga in mangas:
            # noinspection PyUnusedLocal
            finished = bool(manga.find('(finished!)'))
            manga = manga.split('(finished!)')[0].strip()
            if self.dbIO.get_manga(manga) is None:
                self.search(manga)
                # self.threading.exit()

    @staticmethod
    def define_logger():
        """
        define the output of the logger

        :return:
        """
        logger = logging.getLogger()
        log_formatter = logging.Formatter("%(asctime)s [%(threadName)s] [%(levelname)-3.5s]  %(message)s")
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(log_formatter)
        logger.addHandler(console_handler)

    def search(self, title):
        """
        search the modules and update

        :param title:
        :return:
        """
        results = self.search_manager.search_modules(title)
        for res in results:
            if res:
                infos, module_object = res
                title_s, url, chapter_count, latest_chapter, language, module, active = infos
                print "{0:s}({1:s})".format(title, title_s), chapter_count
                if self.systemtray:
                    self.systemtray.notify("Download following manga?",
                                           "%s: chapter count: %s" % (title_s, chapter_count))
                c = raw_input("download this manga?\n")
                if c.lower() in ("y", "yes", "j", "ja"):
                    self.dbIO.add_manga(title, "", language, url, module, active)
                    self.threading.run_once(update.Updater().update)

    def create_systemtray_menu(self):
        self.systemtray.set_icon("bin/icon.ico")
        self.systemtray.set_menu(
            (
                (self.locale.system_tray_search, None, lambda x: self.search(raw_input("Search manga:\n"))),
                ('Send Test Notification', None, lambda x: x.show_notification(
                    self.locale.system_tray_update_notification_title,
                    self.locale.system_tray_update_notification_message % "Test Manga"))
            ),
        )
        self.systemtray.build(on_quit=lambda x: self.exit())


if __name__ == '__main__':
    management = MangaManagement()
