"""
SocksiPy + urllib handler

version: 0.2
author: e<e@tr0ll.in>

This module provides a Handler which you can use with urllib2 to allow it to tunnel your connection through a
socks.sockssocket socket, with out monkey patching the original socket...
"""

import httplib
import urllib2

from management.sessions import socks


class SocksiPyConnection(httplib.HTTPConnection):
    """
    Wrapper class for the socks connection for socks proxies
    """

    def __init__(self, proxytype, proxyaddr, proxyport=None, rdns=True, username=None, password=None, *args, **kwargs):
        """
        initializing function

        :param proxytype:
        :param proxyaddr:
        :param proxyport:
        :param rdns:
        :param username:
        :param password:
        :param args:
        :param kwargs:
        :return:
        """
        self.proxyargs = (proxytype, proxyaddr, proxyport, rdns, username, password)
        httplib.HTTPConnection.__init__(self, *args, **kwargs)

    def connect(self):
        """
        connection function

        :return:
        """
        self.sock = socks.socksocket()
        self.sock.setproxy(*self.proxyargs)
        if isinstance(self.timeout, float):
            self.sock.settimeout(self.timeout)
        self.sock.connect((self.host, self.port))


class SocksiPyHandler(urllib2.HTTPHandler):
    """
    handler of the socks module
    """

    def __init__(self, *args, **kwargs):
        """
        initializing function

        :param args:
        :param kwargs:
        :return:
        """
        self.args = args
        self.kw = kwargs
        urllib2.HTTPHandler.__init__(self)

    def http_open(self, req):
        """
        build and return the connection

        :param req:
        :return:
        """

        def build(host, port=None, strict=None, timeout=0):
            """
            build function

            :param host:
            :param port:
            :param strict:
            :param timeout:
            :return:
            """
            conn = SocksiPyConnection(*self.args, host=host, port=port, strict=strict, timeout=timeout, **self.kw)
            return conn

        return self.do_open(build, req)


if __name__ == "__main__":
    opener = urllib2.build_opener(SocksiPyHandler(socks.PROXY_TYPE_SOCKS4, 'localhost', 9999))
    print opener.open('http://www.whatismyip.com/automation/n09230945.asp').read()
