#!/usr/local/bin/python
# coding: utf-8
import httplib
import os
import random
import socket
import urllib2

import selenium.common
from selenium import webdriver
from selenium.webdriver.phantomjs.webdriver import WebDriver

from management import session

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


# noinspection PyShadowingBuiltins
class PhantomJSWrapper(WebDriver):
    """
    Wrapper for the webdriver object, so no super call, else we have multiple instances of phantomJS
    running at the same time, even after python finished with exit code 0
    additional __del__ function and new functions for readability and reading image files
    """

    # noinspection PyMissingConstructor
    def __init__(self, browser):
        """
        initializing function

        :param browser:
        :return:
        """
        self.__class__ = type(
            browser.__class__.__name__,
            (self.__class__, browser.__class__),
            {})
        self.__dict__ = browser.__dict__

    def __del__(self):
        """
        on delete instance call we close our phantomJS too

        :return:
        """
        self.quit()

    def __setitem__(self, key, value):
        """
        generic variable setter for the browser object
        to guarantee full functionality of the specific session

        :param key:
        :param value:
        :return:
        """
        try:
            setattr(self, key, value)
        except Exception as err:
            print err

    def geturl(self):
        """
        wrapper function for geturl command

        :return:
        """
        return self.current_url

    def read(self, size=-1):
        """
        wrapper function for read command

        :param size:
        :return:
        """

        """
        @deprecated
        image_formats = (".bmp", ".gif", ".jpg", ".png", ".tif", ".jpeg")
        if self.geturl().endswith(image_formats):
            print session.Session.get_image_size(self.geturl())
            self.maximize_window()
            hash = random.getrandbits(128)
            _, ext = os.path.splitext(self.geturl())
            self.save_screenshot(str(hash)+ext)
            image = open(str(hash) + ext, "rb").read()
            os.remove(str(hash) + ext)
            return image
        """

        content_type, width, height = session.Session.get_image_size(
            self.geturl(),
            {"user-agent": self.execute_script("return navigator.userAgent")}
        )
        if "image" in content_type:
            # recognizes jpeg, png, older png and gif image files
            self.set_window_size(width, height)
            hash = random.getrandbits(128)
            _, ext = os.path.splitext(self.geturl())
            self.save_screenshot(str(hash) + ext)
            image = open(str(hash) + ext, "rb").read()
            os.remove(str(hash) + ext)
            return image
        if size:
            return self.page_source[:size]
        return self.page_source

    def readlines(self):
        """
        wrapper function for readlines command

        :return:
        """
        return self.page_source.split("\n")

    def readline(self, index):
        """
        wrapper function for readline command

        :param index:
        :return:
        """
        lines = self.readlines()
        if index < len(lines):
            return lines[index]
        else:
            raise IndexError


# noinspection PyShadowingBuiltins
class PhantomJSSession(object):
    """
    PhantomJS Session using the phantomJS executable under windows
    Allows execution and tracing of Javascript and JQuery functions
    Additional custom wrapper lets us use it like we would use the mechanize Browser or OpenerDirector of urllib2
    Still doesn't support the modification of the accept and request headers
    """

    browser = None
    executable_path = r'%s\phantomjs.exe' % os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir, "bin")
    log_paths = (
        r'%s\ghostdriver.log' % os.path.dirname(os.path.realpath(__file__)),
        r'%s\ghostdriver.log' % os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir),
        r'%s\ghostdriver.log' % os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir, os.pardir)
    )

    def __init__(self):
        """
        initializing function

        :return:
        """
        self.proxy = None
        self.create_session()

    def __del__(self):
        """
        remove logs and close phantomJS on delete function

        :return:
        """
        # module got unloaded at start of __del__ already, reimport it
        import os

        # if browser is active exit it first
        if self.browser:
            self.browser.quit()

        # if log files exist, delete it
        # multiple locations possible depends on where the script who calls this session is located
        for log_file in self.log_paths:
            if os.path.isfile(log_file):
                try:
                    os.remove(log_file)
                except OSError:
                    pass

    def __getattr__(self, name):
        """
        generic methods to call onto the browser object
        to guarantee full functionality of the specific session

        :type name: str
        :return:
        """
        if name in dir(self.browser):
            return getattr(self.browser, name)
        else:
            err = "'%s' object has no attribute '%s'" % (self.__class__.__name__, name)
            raise AttributeError(err)

    def __setitem__(self, key, value):
        """
        generic variable setter for the browser object
        to guarantee full functionality of the specific session

        :param key:
        :param value:
        :return:
        """
        try:
            setattr(self.browser, key, value)
        except Exception as err:
            print err

    def __getitem__(self, key):
        """
        generic variables to retrieve from the browser object
        to guarantee full functionality of the specific session

        :type key: str
        :return:
        """
        if key in dir(self.browser):
            try:
                return getattr(self.browser, key)
            except:
                err = "attribute '%s' could not get returned from '%s'" % (key, self.__class__.__name__)
                raise AttributeError(err)
        else:
            err = "'%s' object has no attribute '%s'" % (self.__class__.__name__, key)
            raise AttributeError(err)

    def open(self, url, timeout_function=None):
        """
        open function with specified timeout handling

        :param url:
        :param timeout_function:
        :return:
        """
        try:
            self.browser.get(url)
            return self.browser
        except (urllib2.URLError, urllib2.HTTPError, socket.timeout, IOError, httplib.HTTPException, socket.error):
            return None
        except selenium.common.exceptions.TimeoutException:
            if timeout_function:
                timeout_function()
            return None

    def get_session(self):
        """
        getter for the session

        :return:
        """
        return self.browser

    def set_proxy(self, ip, type, username='', password=''):
        """
        sets a proxy for the session

        :param ip:
        :param type:
        :param username:
        :param password:
        :return:
        """
        if self.browser:
            self.browser.quit()

        service_args = [
            '--proxy=%s' % ip,
            '--proxy-type=%s' % type,
        ]
        if username:
            service_args.append(
                "--proxy-auth=%s:%s" % (username, password)
            )
        self.proxy = ip
        self.create_session(service_args=service_args)

    def get_proxy(self):
        """
        getter for the session proxy

        :return:
        """
        return self.proxy

    def set_user_agent(self, agent=None):
        """
        sets the user agent, if none is defined, generate a valid random one

        :param agent:
        :return:
        """
        raise NotImplementedError("PhantomJS doesn't support accept header modifications")

    def get_user_agent(self):
        """
        returns the header of user-agent

        :return:
        """
        return self.browser.execute_script("return navigator.userAgent")

    def set_accept_headers(self):
        """
        add the accept headers

        :return:
        """
        raise NotImplementedError("PhantomJS doesn't support accept header modifications")

    def get_headers(self):
        """
        returns all headers

        :return:
        """
        return self.browser.execute_script("return navigator")

    def create_session(self, service_args=None):
        """
        creates the session and defines the most common settings predefined

        :param service_args:
        :return:
        """
        if not service_args:
            service_args = []

        self.browser = PhantomJSWrapper(
            webdriver.PhantomJS(executable_path=self.executable_path, service_args=service_args)
        )
        self.browser.set_page_load_timeout(socket.getdefaulttimeout())


instance = PhantomJSSession

if __name__ == '__main__':
    if session.Session.is_connected():
        socket.setdefaulttimeout(10)
        phantom_session = PhantomJSSession()
        print phantom_session.get_user_agent()
        print phantom_session.open("https://www.google.de").read()
        print phantom_session.open("https://www.google.de").geturl()
        print phantom_session.execute_script("return navigator.userAgent")

        image_test = False
        if image_test:
            open("test.gif", "wb").write(
                phantom_session.open("http://i1142.photobucket.com/albums/"
                                     "n615/tapuy/hestia2-PJM_zps2ag0zzve.gif").read()
            )
            # print phantom_session.get_session().open("https://www.google.de").read()
