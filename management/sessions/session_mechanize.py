#!/usr/local/bin/python
# coding: utf-8

import cookielib
import copy
import httplib
import socket
import urllib2
import warnings

import mechanize

from management import session
from management.sessions import socks

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


# noinspection PyShadowingBuiltins
class MechanizeSession(object):
    """
    session based on mechanize
    """
    browser = None

    def __init__(self):
        """
        initializing function

        :return:
        """
        self.proxy = None
        self.create_session()

    def __getattr__(self, name):
        """
        generic methods to call onto the browser object
        to guarantee full functionality of the specific session

        :type name: str
        :return:
        """
        if name in dir(self.browser):
            return getattr(self.browser, name)
        else:
            err = "'%s' object has no attribute '%s'" % (self.__class__.__name__, name)
            raise AttributeError(err)

    def __setitem__(self, key, value):
        """
        generic variable setter for the browser object
        to guarantee full functionality of the specific session

        :param key:
        :param value:
        :return:
        """
        try:
            setattr(self.browser, key, value)
        except Exception as err:
            print err

    def __getitem__(self, key):
        """
        generic variables to retrieve from the browser object
        to guarantee full functionality of the specific session

        :type key: str
        :return:
        """
        if key in dir(self.browser):
            try:
                return getattr(self.browser, key)
            except:
                err = "attribute '%s' could not get returned from '%s'" % (key, self.__class__.__name__)
                raise AttributeError(err)
        else:
            err = "'%s' object has no attribute '%s'" % (self.__class__.__name__, key)
            raise AttributeError(err)

    def open(self, url, timeout_function=None):
        """
        open function with specified timeout handling

        :param url:
        :param timeout_function:
        :return:
        """
        try:
            content = self.browser.open(url)
            # copy response, since string gets deleted after being read once
            # no deepcopy(file object not possible to deepcopy)
            content_copy = copy.copy(content)
            assert (content_copy.read() not in (None, ""))
            return content
        except (urllib2.URLError, urllib2.HTTPError, socket.timeout, IOError, httplib.HTTPException, socket.error):
            return None
        except AssertionError:
            if timeout_function:
                timeout_function()
            return None

    def get_session(self):
        """
        getter for the session

        :return:
        """
        return self.browser

    def set_proxy(self, ip, type, username='', password=''):
        """
        sets a proxy for the session

        :param ip:
        :param type:
        :param username:
        :param password:
        :return:
        """
        types = {
            "http": socks.PROXY_TYPE_HTTP,
            "socks4": socks.PROXY_TYPE_SOCKS4,
            "socks5": socks.PROXY_TYPE_SOCKS5
        }
        proxy_type = types[type]
        self.proxy = ip

        if ip.count(":"):
            ip, port = ip.split(":")
            port = int(port)
        else:
            # default port
            port = 80

        socket_backup = socket.socket
        connection_backup = socket.create_connection

        # noinspection PyUnusedLocal
        def create_connection(address, timeout=None, source_address=None):
            sock = socks.socksocket()
            sock.connect(address)
            return sock

        # add username and password arguments if proxy authentication required.
        socks.setdefaultproxy(proxy_type, ip, port, username=username, password=password)

        # patch the socket module
        socket.socket = socks.socksocket
        socket.create_connection = create_connection

        self.browser = mechanize.Browser()

        # patch it back
        socket.socket = socket_backup
        socket.create_connection = connection_backup

    def get_proxy(self):
        """
        getter for the session proxy

        :return:
        """
        return self.proxy

    def set_user_agent(self, agent=None):
        """
        sets the user agent, if none is defined, generate a valid random one

        :param agent:
        :return:
        """
        if agent:
            self.browser.addheaders = [("User-agent", agent)]
        else:
            self.browser.addheaders = [("User-agent", session.Session.get_random_useragent())]
        self.set_accept_headers()

    def get_user_agent(self):
        """
        returns the header of user-agent

        :return:
        """
        for header in self.browser.addheaders:
            if header[0].lower() == "user-agent":
                return header[1]

    def set_accept_headers(self):
        """
        add the accept headers

        :return:
        """
        self.browser.addheaders.append(('accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'))
        self.browser.addheaders.append(('accept-language', 'en-US,de-DE;q=0.7,en;q=0.3'))
        self.browser.addheaders.append(('accept-encoding', 'gzip'))

    def get_headers(self):
        """
        returns all headers

        :return:
        """
        return self.browser.addheaders

    def create_session(self):
        """
        creates the session and defines the most common settings predefined

        :return:
        """
        cj = cookielib.LWPCookieJar()
        self.browser = mechanize.Browser()
        self.browser.set_cookiejar(cj)
        self.browser.set_handle_equiv(True)
        # ignore the experimental warning, we don't need it here
        warnings.filterwarnings('ignore', 'gzip transfer encoding is experimental!', )
        self.browser.set_handle_gzip(True)
        self.browser.set_handle_redirect(True)
        self.browser.set_handle_referer(True)
        self.browser.set_handle_robots(False)
        # noinspection PyProtectedMember
        self.browser.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)
        self.set_user_agent()


instance = MechanizeSession

if __name__ == '__main__':
    if session.Session.is_connected():
        mechanize_session = MechanizeSession()
        print mechanize_session.open("https://www.google.de").read()
        print mechanize_session.get_user_agent()
        # print mechanize_session.get_session().open("https://www.google.de").read()

        image_test = False
        if image_test:
            open("test.gif", "wb").write(
                    mechanize_session.open("http://i1142.photobucket.com/albums/"
                                           "n615/tapuy/hestia2-PJM_zps2ag0zzve.gif").read()
            )
