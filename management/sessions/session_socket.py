#!/usr/local/bin/python
# coding: utf-8

import asyncore
import cStringIO
import gzip
import mimetools
import socket
import urlparse

from management import session
from management.sessions import socks

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


class ResponseWrapper(object):
    """
    Wrapper for the response object, for readability without getting unable to re-read the text
    """

    def __init__(self, content):
        magic = bytearray(content)[:2]
        # check for gzip encoding
        if magic == b"\x1f\x8b":
            buf = cStringIO.StringIO(content)
            f = gzip.GzipFile(fileobj=buf)
            self.content = f.read()
        else:
            self.content = content

    def read(self, size=-1):
        """
        wrapper function for read command

        :param size:
        :return:
        """
        if size:
            return self.content[:size]
        return self.content

    def readlines(self):
        """
        wrapper function for readlines command

        :return:
        """
        return self.content.split("\n")

    def readline(self, index):
        """
        wrapper function for readline command

        :param index:
        :return:
        """
        lines = self.readlines()
        if index < len(lines):
            return lines[index]
        else:
            raise IndexError


# noinspection PyPep8Naming
class async_http(asyncore.dispatcher_with_send):
    """
    asynchronous http client
    """

    def __init__(self, host, port, path, consumer, headers=None, data=''):
        """
        initializing function

        :param host:
        :param port:
        :param path:
        :param consumer:
        :param headers:
        :param data:
        :return:
        """
        asyncore.dispatcher_with_send.__init__(self)

        self.host = host
        self.port = port
        self.path = path

        self.data = data
        if not headers:
            self.headers = {}
        else:
            self.headers = headers

        self.consumer = consumer

        self.status = None
        self.header = None

        self.bytes_in = 0
        self.bytes_out = 0

        self.data = ""

        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect((host, port))

    def handle_connect(self):
        """
        handle sucessful connection

        :return:
        """
        text = "GET %s HTTP/1.0\r\nHost: %s\r\n%s\r\n%s\r\n\r\n" % (
            self.path,
            self.host,
            '\r\n'.join('{}: {}'.format(k, v) for k, v in self.headers.items()),
            self.data
        )
        self.send(text)
        self.bytes_out += len(text)

    def handle_expt(self):
        """
        handle failed connection

        :return:
        """
        self.close()
        self.consumer.http_failed(self)

    def handle_read(self):
        """
        reading document and notifying client

        :return:
        """
        data = self.recv(1024)
        self.bytes_in += len(data)

        if not self.header:
            # check if we've seen a full header

            self.data += data

            header = self.data.split("\r\n\r\n", 1)
            if len(header) <= 1:
                return
            header, data = header

            # parse header
            fp = cStringIO.StringIO(header)
            self.status = fp.readline().split(" ", 2)
            self.header = mimetools.Message(fp)

            self.data = ""

            self.consumer.http_header(self)

            if not self.connected:
                return  # channel was closed by consumer

        if data:
            self.consumer.feed(data, self)

    def handle_close(self):
        """
        handle close

        :return:
        """
        self.consumer.close()
        self.close()


# noinspection PyProtectedMember
class HttpClient(object):
    """
    our base client for receiving the important informations from our asynch client
    """
    done = False

    def __init__(self, current_session, length):
        """
        initializing function

        :param current_session:
        :param length:
        :return:
        """
        self.session = current_session
        self.len = length

    # noinspection PyAttributeOutsideInit
    def http_header(self, client):
        """
        receiving the header from the asynch client

        :param client:
        :return:
        """
        self.done = False
        self.data = b''
        self.host = client.host
        # print self.host, repr(client.status)

    # noinspection PyUnusedLocal
    def http_failed(self, client):
        """
        receiving the failed notification on no sucessful connection

        :param client:
        :return:
        """
        # print self.host, "failed"
        self.done = True

    def feed(self, data, asynch_client):
        """
        while we can read the document feed the data
        to our data variable

        :param data:
        :param asynch_client:
        :return:
        """
        if self.len and (len(self.data) >= self.len):
            # noinspection PyAttributeOutsideInit
            self.data = self.data[:self.len]
            asynch_client.handle_close()
            self.close()
        self.data += data

    def close(self):
        """
        close our client

        :return:
        """
        self.done = True
        self.session._done(self.data)


# noinspection PyShadowingNames,PyShadowingBuiltins
class SocketSession(object):
    """
    our own socket client without the
    usage of urllib or external modules
    """
    readable = False
    url = ''
    headers = {}
    proxy = ''

    def __init__(self):
        """
        initializing function

        :return:
        """
        self.set_accept_headers()
        self.set_user_agent()

    def get_url(self):
        """
        getter for the url

        :return:
        """
        return self.url

    def open(self, url, headers=None, data='', len=None):
        """
        open function for the asynch socket client

        :param url:
        :param headers:
        :param data:
        :param len:
        :return:
        """
        if not headers:
            headers = self.headers
        self.url = url
        http_client = HttpClient(self, length=len)
        self.readable = False
        # turn the uri into a valid request
        scheme, host, path, params, query, fragment = urlparse.urlparse(url)
        assert scheme == "http", "only supports HTTP requests"
        try:
            host, port = host.split(":", 1)
            port = int(port)
        except (TypeError, ValueError):
            port = 80  # default port
        if not path:
            path = "/"
        if params:
            path = path + ";" + params
        if query:
            path = path + "?" + query

        async_http(host, port, path, http_client, headers, data)
        asyncore.loop()

        while not self.readable:
            pass
        return ResponseWrapper(self.data)

    def _done(self, data):
        """
        marks done status

        :param data:
        :return:
        """
        self.readable = True
        self.data = data

    def set_proxy(self, ip, type, username='', password=''):
        """
        sets a proxy for the session

        :param ip:
        :param type:
        :param username:
        :param password:
        :return:
        """
        types = {
            "http": socks.PROXY_TYPE_HTTP,
            "socks4": socks.PROXY_TYPE_SOCKS4,
            "socks5": socks.PROXY_TYPE_SOCKS5
        }
        proxy_type = types[type]
        self.proxy = ip

        if ip.count(":"):
            ip, port = ip.split(":", 1)
            port = int(port)
        else:
            # default port
            port = 80

        # noinspection PyUnusedLocal
        def create_connection(address, timeout=None, source_address=None):
            sock = socks.socksocket()
            sock.connect(address)
            return sock

        # add username and password arguments if proxy authentication required.
        socks.setdefaultproxy(proxy_type, ip, port, username=username, password=password)

        # patch the socket module
        socket.socket = socks.socksocket
        socket.create_connection = create_connection

    def get_proxy(self):
        """
        returns a proxy for the session

        :return:
        """
        return self.proxy

    # noinspection PyMethodMayBeStatic
    def get_session(self):
        """
        returns a proxy for the session

        :return:
        """
        raise EnvironmentError("Unable to get the socket object, running in asynch thread")

    def get_headers(self):
        """
        returns the headers for the session

        :return:
        """
        return self.headers

    def set_user_agent(self, agent=None):
        """
        sets the user agent for the session

        :param agent:
        :return:
        """
        if agent:
            self.headers.setdefault("User-agent", agent)
        else:
            self.headers.setdefault("User-agent", session.Session.get_random_useragent())

    def get_user_agent(self):
        """
        returns the user-agent for the session

        :return:
        """
        if "User-agent" in self.headers:
            return self.headers["User-agent"]
        else:
            return None

    def set_accept_headers(self):
        """
        set standard accept headers

        :return:
        """
        self.headers.setdefault('accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
        self.headers.setdefault('accept-language', 'en-US,de-DE;q=0.7,en;q=0.3')
        # enable gzip compression with the wrapper class, saving up to 2/3 of the traffic
        self.headers.setdefault('accept-encoding', '')


instance = SocketSession

if __name__ == '__main__':
    url = "http://stackoverflow.com"

    import time

    s = time.time()
    session_object = SocketSession()
    session_object.set_user_agent("Mozilla/5.4 (Windows; U; Windows NT 5.1; en-US; rv:3.9.0.2)"
                                  " Gecko/2092094033 Firefox/4.8.0.1")
    print session_object.open(url).read()
    print time.time() - s

    if False:
        open("test_socket.gif", "wb").write(
            session_object.open("http://i.epvpimg.com/qC6Gb.gif").read()
        )
