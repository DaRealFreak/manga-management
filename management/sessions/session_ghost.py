#!/usr/local/bin/python
# coding: utf-8

import gzip
from StringIO import StringIO

import ghost

from management import session

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


class GhostWrapper(ghost.ghost.HttpResource):
    """
    Wrapper for the HttpRessource object, so no super call, just for unifying the session modules
    additional new functions for readability
    """

    # noinspection PyMissingConstructor
    def __init__(self, page):
        """
        initializing function

        :param page:
        :return:
        """
        self.__class__ = type(
            page.__class__.__name__,
            (self.__class__, page.__class__),
            {})
        self.__dict__ = page.__dict__
        self.page = page

    def __del__(self):
        """
        remove additional variables defined by the wrapper

        :return:
        """
        del self.page

    def read(self, size=-1):
        """
        wrapper function for read command

        :param size:
        :return:
        """
        magic = bytearray(self.content)[:2]
        # check for gzip encoding
        if magic == b"\x1f\x8b":
            buf = StringIO(self.content)
            f = gzip.GzipFile(fileobj=buf)
            self.content = f.read()
        if size:
            return self.content[:size]
        return self.content

    def readlines(self):
        """
        wrapper function for readlines command

        :return:
        """
        return self.content.split("\n")

    def readline(self, index):
        """
        wrapper function for readline command

        :param index:
        :return:
        """
        lines = self.readlines()
        if index < len(lines):
            return lines[index]
        else:
            raise IndexError


# noinspection PyUnresolvedReferences,PyShadowingBuiltins
class GhostSession(object):
    """
    session based on the ghost extension
    """
    browser = None
    headers = {}

    def __init__(self):
        self.proxy = None
        self.create_session()

    def __del__(self):
        self.browser.exit()

    def __getattr__(self, name):
        """
        generic methods to call onto the browser object
        to guarantee full functionality of the specific session

        :type name: str
        :return:
        """
        if name in dir(self.browser):
            return getattr(self.browser, name)
        else:
            err = "'%s' object has no attribute '%s'" % (self.__class__.__name__, name)
            raise AttributeError(err)

    def __setitem__(self, key, value):
        """
        generic variable setter for the browser object
        to guarantee full functionality of the specific session

        :param key:
        :param value:
        :return:
        """
        try:
            setattr(self, key, value)
        except Exception as err:
            print err

    def __getitem__(self, key):
        """
        generic variables to retrieve from the browser object
        to guarantee full functionality of the specific session

        :type key: str
        :return:
        """
        if key in dir(self.browser):
            try:
                return getattr(self.browser, key)
            except:
                err = "attribute '%s' could not get returned from '%s'" % (key, self.__class__.__name__)
                raise AttributeError(err)
        else:
            err = "'%s' object has no attribute '%s'" % (self.__class__.__name__, key)
            raise AttributeError(err)

    def save_screenshot(self, fpath):
        """
        save screenshot to file

        :param fpath:
        :return:
        """
        self.browser.capture_to(fpath)

    def open(self, url, timeout_function=None):
        """
        open function with specified timeout handling

        :param url:
        :param timeout_function:
        :return:
        """
        try:
            # extra resources are f.e. loaded js libraries
            image_formats = (".bmp", ".gif", ".jpg", ".png", ".tif", ".jpeg")
            if url.endswith(image_formats):
                # allow twice the time for images, since ghost isn't the fastest module
                self.browser.open(url, headers=self.headers, timeout=self.browser.wait_timeout * 2)
            page, extra_resources = self.browser.open(url, headers=self.headers)
            return GhostWrapper(page)
        except (urllib2.URLError, urllib2.HTTPError, socket.timeout, IOError, httplib.HTTPException, socket.error):
            return None
        except ghost.Error:
            if timeout_function:
                timeout_function()
            return None

    def execute_script(self, script):
        """
        execute javascript script

        :param script:
        :return:
        """
        return self.browser.evaluate(script)

    def get_session(self):
        """
        getter for the session

        :return:
        """
        return self.browser

    def set_proxy(self, ip, type, username='', password=''):
        """
        sets a proxy for the session

        :param ip:
        :param type:
        :param username:
        :param password:
        :return:
        """
        if ip.count(":"):
            ip, port = ip.split(":")
            port = int(port)
        else:
            # default port
            port = 80

        spookey = ghost.Ghost()
        if self.browser:
            self.browser = ghost.Session(spookey, user_agent=self.browser.user_agent)
        else:
            self.browser = ghost.Session(spookey)
        self.browser.wait_timeout = 15
        self.browser.set_proxy(type, ip, port, username, password)

    def get_proxy(self):
        """
        getter for the session proxy

        :return:
        """
        return self.proxy

    def set_user_agent(self, agent=None):
        """
        sets the user agent, if none is defined, generate a valid random one

        :param agent:
        :return:
        """
        if agent:
            self.browser.user_agent = [("User-agent", agent)]
        else:
            self.browser.user_agent = [("User-agent", session.Session.get_random_useragent())]
        self.set_accept_headers()

    def get_user_agent(self):
        """
        returns the header of user-agent

        :return:
        """
        if self.browser.user_agent:
            return self.browser.user_agent[0][1]
        else:
            return ""

    def set_accept_headers(self):
        """
        add the accept headers

        :return:
        """
        self.headers.setdefault('accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
        self.headers.setdefault('accept-language', 'en-US,de-DE;q=0.7,en;q=0.3')
        # enable gzip compression with the wrapper class, saving up to 2/3 of the traffic
        self.headers.setdefault('accept-encoding', 'gzip')

    def get_headers(self):
        """
        returns all headers

        :return:
        """
        return self.headers

    def create_session(self):
        """
        creates the session and defines the most common settings predefined

        :return:
        """
        browser = ghost.Ghost()
        self.browser = ghost.Session(browser, download_images=True)
        self.set_user_agent()


instance = GhostSession

if __name__ == '__main__':
    if session.Session.is_connected():
        ghost_session = GhostSession()
        print ghost_session.open("https://www.google.de").read()
        print ghost_session.get_user_agent()

        image_test = True
        if image_test:
            open("test.gif", "wb").write(
                ghost_session.open("http://i.epvpimg.com/qC6Gb.gif").read()
            )
