#!/usr/local/bin/python
# coding: utf-8

import cookielib
import gzip
import httplib
import socket
import urllib2
from StringIO import StringIO
# used for socks4/socks5 proxy

from management import session
from management.sessions import socks
from management.sessions import socksipyhandler

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


# noinspection PyProtectedMember
class UrllibWrapper(socket._fileobject):
    """
    Wrapper for the socket._fileobject object, so no super call, just for unifying the session modules.
    additional new functions for readability and re-readability of the content source
    """

    # noinspection PyMissingConstructor
    def __init__(self, content):
        """
        initializing function

        :param content:
        :return:
        """
        self.__class__ = type(
            content.__class__.__name__,
            (self.__class__, content.__class__),
            {})
        self.__dict__ = content.__dict__

        self.content = content
        self.page_content = None
        self.origin_read = self.read
        self.read = self._read

    def __del__(self):
        """
        on delete replace the read function with the original instance

        :return:
        """
        self.read = self.origin_read

    def _read(self, size=-1):
        """
        wrapper function for read command

        :param size:
        :return:
        """
        if not self.page_content:
            self.page_content = self.origin_read(size)

        magic = bytearray(self.page_content)[:2]
        # check for gzip encoding
        if magic == b"\x1f\x8b":
            buf = StringIO(self.page_content)
            f = gzip.GzipFile(fileobj=buf)
            self.page_content = f.read()
        return self.page_content


# noinspection PyShadowingBuiltins
class UrllibSession(object):
    """
    session based on urllib
    """
    browser = None

    def __init__(self):
        self.proxy = None
        self.create_session()

    def __getattr__(self, name):
        """
        generic methods to call onto the browser object
        to guarantee full functionality of the specific session

        :type name: str
        :return:
        """
        if name in dir(self.browser):
            return getattr(self.browser, name)
        else:
            err = "'%s' object has no attribute '%s'" % (self.__class__.__name__, name)
            raise AttributeError(err)

    def __setitem__(self, key, value):
        """
        generic variable setter for the browser object
        to guarantee full functionality of the specific session

        :param key:
        :param value:
        :return:
        """
        try:
            setattr(self.browser, key, value)
        except Exception as err:
            print err

    def __getitem__(self, key):
        """
        generic variables to retrieve from the browser object
        to guarantee full functionality of the specific session

        :type key: str
        :return:
        """
        if key in dir(self.browser):
            try:
                return getattr(self.browser, key)
            except:
                err = "attribute '%s' could not get returned from '%s'" % (key, self.__class__.__name__)
                raise AttributeError(err)
        else:
            err = "'%s' object has no attribute '%s'" % (self.__class__.__name__, key)
            raise AttributeError(err)

    def open(self, url, timeout_function=None):
        """
        open function with specified timeout handling

        :param url:
        :param timeout_function:
        :return:
        """
        try:
            # timeout * 10 results in the seconds after which the timeout will occur
            # absolutely no idea why, but we need the timeout parameter here to let it work correctly
            content = self.browser.open(url, timeout=float(socket.getdefaulttimeout()) / 10)
            if content.getcode() in (200, 201, 202, 203, 205, 206, 207, 226):
                return UrllibWrapper(content)
        except (urllib2.HTTPError, socket.timeout, IOError, httplib.HTTPException, socket.error):
            return None
        except urllib2.URLError:
            if timeout_function:
                timeout_function()
            return None

    def get_session(self):
        """
        getter for the session

        :return:
        """
        return self.browser

    def set_proxy(self, ip, type, username='', password=''):
        """
        sets a proxy for the session

        :param ip:
        :param type:
        :param username:
        :param password:
        :return:
        """
        types = {
            "http": socks.PROXY_TYPE_HTTP,
            "socks4": socks.PROXY_TYPE_SOCKS4,
            "socks5": socks.PROXY_TYPE_SOCKS5
        }
        proxy_type = types[type]
        self.proxy = ip

        if ip.count(":"):
            ip, port = ip.split(":")
            port = int(port)
        else:
            # default port
            port = 80
        cookieprocessor = urllib2.HTTPCookieProcessor(cookielib.LWPCookieJar())
        self.browser = urllib2.build_opener(
            cookieprocessor, socksipyhandler.SocksiPyHandler(proxy_type, ip, port, username=username, password=password)
        )

    def get_proxy(self):
        """
        getter for the session proxy

        :return:
        """
        return self.proxy

    def set_user_agent(self, agent=None):
        """
        sets the user agent, if none is defined, generate a valid random one

        :param agent:
        :return:
        """
        if agent:
            self.browser.addheaders = [("User-agent", agent)]
        else:
            self.browser.addheaders = [("User-agent", session.Session.get_random_useragent())]
        self.set_accept_headers()

    def get_user_agent(self):
        """
        returns the header of user-agent

        :return:
        """
        for header in self.browser.addheaders:
            if header[0].lower() == "user-agent":
                return header[1]

    def set_accept_headers(self):
        """
        add the accept headers

        :return:
        """
        self.browser.addheaders.append(('accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'))
        self.browser.addheaders.append(('accept-language', 'en-US,de-DE;q=0.7,en;q=0.3'))
        # enable gzip compression with the wrapper class, saving up to 2/3 of the traffic
        self.browser.addheaders.append(('accept-encoding', 'gzip'))

    def get_headers(self):
        """
        returns all headers

        :return:
        """
        return self.browser.addheaders

    def create_session(self):
        """
        creates the session and defines the most common settings predefined

        :return:
        """
        cookieprocessor = urllib2.HTTPCookieProcessor(cookielib.LWPCookieJar())
        self.browser = urllib2.build_opener(cookieprocessor)
        self.set_user_agent()


instance = UrllibSession

if __name__ == '__main__':
    if session.Session.is_connected():
        socket.setdefaulttimeout(10)
        urllib_session = UrllibSession()
        print urllib_session.open("https://www.google.de").read()
        print urllib_session.get_user_agent()
        # print urllib_session.get_session().open("https://www.google.de").read()
