import os
import re

try:
    from management import session
except ImportError:
    session = None

import urlparse
import urllib
import HTMLParser
from pprint import pprint
from management import decorators


@decorators.deprecated
class Taadd(object):
    translated = True
    language = 'en'

    _update_function = None
    _current_manga = None
    _session = None

    _html_escape_table = {
        "&": "&amp;",
        '"': "&quot;",
        "'": "&apos;",
        ">": "&gt;",
        "<": "&lt;",
    }
    _html_parser = HTMLParser.HTMLParser()

    def __init__(self):
        if __name__ != '__main__':
            if not self._session:
                self._session = session.SessionManager()

    def is_translated(self):
        return self.translated

    def get_language(self):
        return self.language

    def set_session(self, session):
        self._session = session

    def set_update_function(self, func):
        self._update_function = func

    def _html_escape(self, text):
        return "".join(self._html_escape_table.get(c, c) for c in text)

    def _get_title(self, title):
        return urllib.quote_plus(self._html_escape(title))

    """
    search for the manga title, returning a 
    fitting title key if manga is found
    """

    def search_manga(self, title):
        """
        From RFC 1738 specification:
        unreserved  = ALPHA / DIGIT / "-" / "." / "_" / "~"     <---This seems like a practical shortcut, most closely
        resembling original answer
        reserved    = gen-delims / sub-delims
        gen-delims  = ":" / "/" / "?" / "#" / "[" / "]" / "@"
        sub-delims  = "!" / "$" / "&" / "'" / "(" / ")"
                      / "*" / "+" / "," / ";" / "="
        """
        search_title = re.findall('([\w\-\._~ ]+)', title)
        if search_title:
            title = search_title[0]

        search_url = 'http://search.taadd.com/search/?wd=%s' % urllib.quote_plus(title)
        html_object = self._session.open(search_url)
        if not html_object:
            print "connection problems to the site, skipping"
            return None, 0, "", self.translated
        html_content = html_object.read()

        pattern = re.compile('<a title="(.+?)\s?((Vol(\d+)\.)?\s?Ch.(\d+))?"\shref="(.+)/">', re.IGNORECASE)
        results = pattern.findall(html_content)
        if results:
            name, latest_chapter, _, _, chapter_count, url = results[0]
            url += "/"
            latest_chapter = "%s %s" % (name, latest_chapter)
            if not chapter_count:
                chapter_count = " ".join(name.split()[-1:])
                name = " ".join(name.split()[:-1])
            # remove possible volume and chapter indentifiers
            return (self._html_parser.unescape(" ".join(name.split())),
                    int(float(chapter_count)),
                    self._html_parser.unescape(" ".join(latest_chapter.split())),
                    self.translated)
        else:
            return "", 0, "", self.translated

    def get_overview_page(self, manga):
        """
        create key for url based on title and 
        return the html content of the main page
        """
        self._current_manga = self._html_parser.unescape(manga)
        url = "http://www.taadd.com/book/%s.html?waring=1" % self._get_title(manga)
        url_content = self._session.open(url)
        if url_content:
            return url_content.read()
        else:
            return ''

    def get_latest_chapter(self, html_content):
        """
        check for latest chapter and return the title
        """
        if not html_content:
            return "404_site_content"
        pattern = re.compile('<td align="left" width=[\d]+%><a href=".*">(.*)</a></td>')
        result = pattern.findall(html_content)
        if result:
            index = result[0]
            return "%s" % (self._html_parser.unescape(index))
        else:
            return "Pattern misfit, please check the function get_latest_chapter"

    def fetch_new_chapters(self, current_chapter, html_content):
        """
        based on current chapter, fetch the newer chapters
        and return the title and a list of urls/paths of the images
        """
        if not self._current_manga:
            raise AttributeError('No manga defined, '
                                 'please get the html content with the function get_overview to define the title')

        pattern = re.compile('<td align="left" width=[\d]+%><a href="(.*)">(.*)</a></td>')
        result = pattern.findall(html_content)

        if result:
            # local variable needed, since we access this class multiple times at the same time
            # but safe to clone it here, since iteration takes longer than regex check
            current_manga = self._current_manga
            sorted_list = list()
            for url, index in result:
                if current_chapter == self._html_parser.unescape(index):
                    break
                sorted_list.append((url, index))
            sorted_list.reverse()

            for url, index in sorted_list:
                index = self._html_parser.unescape(index)
                images = self._extract_images(url, index)
                if self._update_function:
                    self._update_function({index: (current_manga, index, images)})
                else:
                    print "No update function defined, please define an update function beforehand."

    def _extract_images(self, url, path):

        def fetch_pages_from_chapter(url, path):
            """
            function to check the page jump dialog for the sites to extract the site urls
            iterating through the sites to extract the images afterwards
            """
            pattern = re.compile('<option value="(.*)"       >([1-9]\d{0,3})</option>')
            url = urlparse.urljoin("http://www.taadd.com", url)
            html_content = self._session.open(url).read()

            result = pattern.findall(html_content)
            pages = list()
            if result:
                for page_url, index in result:
                    pages.append((page_url, os.path.join(path, index)))
                pages.insert(0, (url, os.path.join(path, "1")))
            return pages

        pages = fetch_pages_from_chapter(url, path)
        pattern = re.compile("<img id='comicpic' name='comicpic' src=\"(.*)\" onclick")

        images = list()
        while pages:
            # pop the element of the queue
            url, file_path = pages.pop()
            html_content = self._session.open(url).read()
            result = pattern.findall(html_content)
            if result:
                url = result[0]
                file_type = url.split('.')[-1]
                images.append((url, "%s.%s" % (file_path, file_type)))
        images.reverse()
        return images


__main_class__ = Taadd

if __name__ == '__main__':
    import urllib2
    import cookielib

    dummy_session = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookielib.CookieJar()))
    dummy_session.addheaders = [('User-agent', 'Mozilla/5.0')]

    # test_manga = 'Myuu & I'
    test_manga = 'Ore ga Ojou-sama Gakkou ni "Shomin Sample" Toshite Rachirareta Ken'
    site = __main_class__()
    site.set_update_function(lambda x: pprint(x))
    site.set_session(dummy_session)
    real_name, count, latest_chapter, translated = site.search_manga(test_manga)
    print (real_name, count, latest_chapter)
    print site.get_latest_chapter(site.get_overview_page(real_name))
    site.fetch_new_chapters('Ore ga Ojou-sama Gakkou ni "Shomin Sample" Toshite Rachirareta Ken 14',
                            site.get_overview_page(test_manga))
