import os
import re

try:
    from management import session
except ImportError:
    session = None

import imp
import urllib
import logging
from pprint import pprint
# TODO:
# check with "Darwin's game"
# error log:
# 2014-10-06 05:44:20,197 [UpdateThread] [WARNI]  Exception: near "s": syntax error

from management import decorators


@decorators.deprecated
class MangaReader(object):
    base_url = 'http://www.mangareader.net'
    translated = True
    language = 'en'

    _title_pattern = re.compile("[^\w\- ]")
    _update_function = None
    _current_manga = None
    _session = None
    _logger = None

    def __init__(self):
        if __name__ != '__main__':
            if not self._session:
                self._session = session.SessionManager()
        if not self._logger:
            self._logger = logging.getLogger()

    def is_translated(self):
        return self.translated

    def get_language(self):
        return self.language

    def set_session(self, session):
        self._session = session

    def set_update_function(self, func):
        self._update_function = func

    def _get_title(self, title):
        return self._title_pattern.sub('', title).replace(' ', '-')

    def search_manga(self, title):
        """
        search for the manga title, returning a 
        fitting title key if manga is found
        """
        search_url = '%s/search/?w=%s' % (self.base_url, title.replace(' ', '+'))
        html_object = self._session.open(search_url)
        if not html_object:
            print "connection problems to the site, skipping"
            return None, 0, "", self.translated
        html_content = html_object.read()

        # unique header for the results, so only the results are getting displayed
        search_results = re.findall('<h3><a href="(.*)">(.*)</a></h3>', html_content)
        if search_results:
            url, name = search_results[0]
            # index in regex is possible because it's in the same section as the search_result, which got checked
            chapter_count = re.findall('<div class="chapter_count">([\d]+).*</div>', html_content)[0]
            latest_chapter = self.get_latest_chapter(self._session.open(self.base_url + url).read())
            return name, int(chapter_count), latest_chapter, self.translated
        else:
            return None, 0, "", self.translated

    def get_overview_page(self, manga):
        """
        create key for url based on title and 
        return the html content of the main page
        """
        # special mangas got another url with index between and direct html link
        # ordinary created link by this is delayed by an unknown timespan
        self._current_manga = manga
        url = "%s/%s" % (self.base_url, self._get_title(manga).lower())
        url_content = self._session.open(url)
        if url_content:
            return url_content.read()
        else:
            return ''

    @staticmethod
    def get_latest_chapter(html_content):
        """
        check for latest chapter and return the title
        """
        if not html_content:
            return "404_site_content"
        pattern = re.compile('<a href="/.*/.*">(.*)</a> : (.*)</td>\n<td>(.*)</td>')
        result = pattern.findall(html_content)
        if result:
            index, chapter, date = result[-1]
            return str(index)
        # return "%s: %s" % (index, chapter)
        else:
            return "Pattern misfit, please check the function get_latest_chapter"

    def fetch_new_chapters(self, current_chapter, html_content):
        """
        based on current chapter, fetch the newer chapters
        and return the title and a list of urls/paths of the images
        """
        pattern = re.compile('<a href="(/.*/.*)">(.*)</a> : (.*)</td>\n<td>(.*)</td>')
        result = pattern.findall(html_content)

        if result:
            # local variable needed, since we access this class multiple times at the same time
            # but safe to clone it here, since iteration takes longer than regex check
            current_manga = self._current_manga
            sorted_list = list()
            # reverse list to check for newer chapters
            for url, index, chapter, date in result[::-1]:
                if current_chapter == index:
                    break
                # post_time = time.mktime(datetime.datetime.strptime(date, "%m/%d/%Y").timetuple())
                url = self.base_url + url
                sorted_list.append((url, index, chapter, date))
            sorted_list.reverse()

            for url, index, chapter, date in sorted_list:
                images = self._extract_images(url, index)
                if self._update_function:
                    self._update_function({index: (current_manga, index, images)})
                else:
                    self._logger.warn('No update function defined, please define an update function beforehand.')

    def _extract_images(self, url, path):

        def fetch_pages_from_chapter(url, path):
            """
            function to check the page jump dialog for the sites to extract the site urls
            iterating through the sites to extract the images afterwards
            """
            pattern = re.compile('<option value="([^\s]+)">([\d]{1,3})</option>')
            html_content = self._session.open(url).read()

            result = pattern.findall(html_content)
            pages = list()
            if result:
                pages.append((url, os.path.join(path, "1")))
                for url, file_path in result:
                    pages.append((self.base_url + url, os.path.join(path, file_path)))
            return pages

        if not self._session:
            raise AttributeError('No session set in module: "mangareader"')

        pages = fetch_pages_from_chapter(url, path)
        pages.reverse()
        pattern = re.compile('<a href="[^\s]+"><img id="img" width="[\d]+"'
                             ' height="[\d]+" src="([^\s]+)" alt=".*" name="img" /></a>')

        images = list()
        while len(pages) > 0:
            # pop the element of the queue
            url, file_path = pages.pop()
            try:
                html_content = self._session.open(url).read()
            except:
                self._session = session.SessionManager().setup_session()
                html_content = self._session.open(url).read()
            result = pattern.findall(html_content)
            if result:
                url = result[0]
                file_type = url.split('.')[-1]
                images.append((url, "%s.%s" % (file_path, file_type)))
        return images


__main_class__ = MangaReader

if __name__ == '__main__':
    dummy_session = imp.new_module("session")
    dummy_session.open = urllib.urlopen

    test_manga = "Darwin's Game"
    site = __main_class__()
    site.set_update_function(lambda x: pprint(x))
    site.set_session(dummy_session)
    print site.search_manga(test_manga)
    print site.get_latest_chapter(site.get_overview_page(test_manga))
    site.fetch_new_chapters("", site.get_overview_page(test_manga))
