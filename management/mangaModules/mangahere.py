import os
import re
from StringIO import StringIO

try:
    from management import session
except ImportError:
    session = None

import urllib
import logging
import gzip
from management import decorators


@decorators.deprecated
class MangaHere(object):
    translated = True
    language = 'en'

    _title_pattern = re.compile("[^\w]")
    _update_function = None
    _current_manga = None
    _session = None
    _logger = None

    def __init__(self):
        if __name__ != '__main__':
            if not self._session:
                self._session = session.SessionManager()
        if not self._logger:
            self._logger = logging.getLogger()

    def is_translated(self):
        return self.translated

    def get_language(self):
        return self.language

    def set_session(self, session):
        self._session = session

    def set_update_function(self, func):
        self._update_function = func

    def _get_title(self, title):
        title = self._title_pattern.sub('_', title)
        while title.endswith('_'):
            title = title[:-1]
        while title.count('__'):
            title = title.replace('__', '_')
        return title

    def _get_search_title(self, title):
        title = self._title_pattern.sub('+', title)
        while title.endswith('+'):
            title = title[:-1]
        while title.count('++'):
            title = title.replace('++', '+')
        return title

    def search_manga(self, title):
        """
        search for the manga title, returning a 
        fitting title key if manga is found
        """
        # works only with ALL parameters, all on standard, only keyword search
        search_url = ('http://www.mangahere.co/search.php?name=%s' % (self._get_search_title(title)))
        html_object = self._session.open(search_url)
        if not html_object:
            print "connection problems to the site, skipping"
            return None, 0, "", self.translated
        html_content = html_object.read()

        # unique header for the results, so only the results are getting displayed
        pattern = re.compile('<a href="(.*)" class="manga_info name_one"')
        result = re.findall(pattern, html_content)
        if not result:
            return None, 0, "", self.translated

        first_result = result[0]
        mobile_url = first_result.replace('www.mangahere.co', 'm.mangahere.co')
        html_content = self._session.open(mobile_url).read()
        chapters = re.findall('<a href="(http://m.mangahere.co/manga/.*)">C.([\d\.\-]+)</a>', html_content)

        if chapters:
            return title, len(chapters), "%s %s" % (title, chapters[0][1]), self.translated
        return None, 0, "", self.translated

    def get_overview_page(self, manga):
        """
        create key for url based on title and 
        return the html content of the main page
        """
        self._current_manga = manga
        url = "http://m.mangahere.co/manga/%s" % self._get_title(manga).lower()
        url_content = self._session.open(url)
        if url_content:
            return url_content.read()
        else:
            return ''

    def get_latest_chapter(self, html_content):
        """
        check for latest chapter and return the title
        """
        if not html_content:
            return "404_site_content"
        pattern = re.compile('<a href="http://m.mangahere.co/manga/.*">C.([\d\.\-]+)</a>')
        result = pattern.findall(html_content)
        if result:
            index = result[0]
            return "%s %s" % (self._current_manga, index)
        else:
            return "Pattern misfit, please check the function get_latest_chapter"

    def fetch_new_chapters(self, current_chapter, html_content):
        """
        based on current chapter, fetch the newer chapters
        and return the title and a list of urls/paths of the images
        """
        if not self._current_manga:
            raise AttributeError('No manga defined, '
                                 'please get the html content with the function get_overview to define the title')

        pattern = re.compile('<a href="(http://m.mangahere.co/manga/.*)">C.([\d\.\-]+)</a>')
        result = pattern.findall(html_content)

        if result:
            # local variable needed, since we access this class multiple times at the same time
            # but safe to clone it here, since iteration takes longer than regex check
            current_manga = self._current_manga
            sorted_list = list()
            for url, index in result:
                if current_chapter == index:
                    break
                sorted_list.append((url, index))
            sorted_list.reverse()

            for url, index in sorted_list:
                images = self._extract_images(url, index)
                if self._update_function:
                    self._update_function({index: (current_manga, index, images)})
                else:
                    self._logger.warn('No update function defined, please define an update function beforehand.')

    """
    extract images from the specific urls and un-gzip the html content is it is zipped
    the page doesn't check for accept encoding headers
    """

    def _extract_images(self, url, path):
        # mobile view uses gzip!
        images = list()
        mobile_view_url = url.replace('mangahere.co/manga/', 'mangahere.co/roll_manga/')

        html_content = self._session.open(mobile_view_url)
        if html_content.info().get('Content-Encoding') == 'gzip':
            buf = StringIO(html_content.read())
            f = gzip.GzipFile(fileobj=buf)
            html_content = f.read()
        else:
            html_content = html_content.read()

        pattern = re.compile('<img class="lazy" data-original="(.*)" />')
        result = pattern.findall(html_content)

        if result:
            for page in result:
                page_index = result.index(page) + 1
                page = page[:page.find('?')]
                file_path = os.path.join(path, str(page_index))
                file_type = page.split('.')[-1]
                images.append((page, "%s %s.%s" % (self._current_manga, file_path, file_type)))
        return images


__main_class__ = MangaHere

if __name__ == '__main__':
    from pprint import pprint

    dummy_session = urllib
    dummy_session.open = urllib.urlopen

    test_manga = "Monster Musume no Iru Nichijou"
    site = __main_class__()
    site.set_update_function(lambda x: pprint(x))
    site.set_session(dummy_session)
    print site.search_manga(test_manga)
    print site.get_latest_chapter(site.get_overview_page(test_manga))
    site.fetch_new_chapters("Monster Musume no Iru Nichijou 24", site.get_overview_page(test_manga))
