import os
import re

try:
    from management import session
except ImportError:
    session = None

import urllib
import logging
from pprint import pprint
from management import decorators


@decorators.deprecated
class MangaFox(object):
    translated = True
    language = 'en'

    _title_pattern = re.compile("[^\w]")
    _update_function = None
    _current_manga = None
    _session = None
    _logger = None

    def __init__(self):
        if __name__ != '__main__':
            if not self._session:
                self._session = session.SessionManager()
        if not self._logger:
            self._logger = logging.getLogger()

    def is_translated(self):
        return self.translated

    def get_language(self):
        return self.language

    def set_session(self, session):
        self._session = session

    def set_update_function(self, func):
        self._update_function = func

    def _get_title(self, title):
        title = self._title_pattern.sub('_', title)
        while title.endswith('_'):
            title = title[:-1]
        while title.count('__'):
            title = title.replace('__', '_')
        return title

    def search_manga(self, title):
        """
        search for the manga title, returning a 
        fitting title key if manga is found
        """
        # works only with ALL parameters, all on standard, only keyword search
        search_url = ('http://mangafox.me/search.php?name_method=cw&name=%s&type=&author_method=cw&author='
                      '&artist_method=cw&artist=&genres[Action]=0&genres[Adult]=0&genres[Adventure]=0&genres[Comedy]=0'
                      '&genres[Doujinshi]=0&genres[Drama]=0&genres[Ecchi]=0&genres[Fantasy]=0&genres[Gender+Bender]=0'
                      '&genres[Harem]=0&genres[Historical]=0&genres[Horror]=0&genres[Josei]=0&genres[Martial+Arts]=0'
                      '&genres[Mature]=0&genres[Mecha]=0&genres[Mystery]=0&genres[One+Shot]=0&genres[Psychological]=0'
                      '&genres[Romance]=0&genres[School+Life]=0&genres[Sci-fi]=0&genres[Seinen]=0&genres[Shoujo]=0'
                      '&genres[Shoujo+Ai]=0&genres[Shounen]=0&genres[Shounen+Ai]=0&genres[Slice+of+Life]=0&genres[Smut]'
                      '=0&genres[Sports]=0&genres[Supernatural]=0&genres[Tragedy]=0&genres[Webtoons]=0&genres[Yaoi]=0'
                      '&genres[Yuri]=0&released_method=eq&released=&rating_method=eq&rating=&is_completed=&advopts=1'
                      % (self._get_title(title)))
        html_object = self._session.open(search_url)
        if not html_object:
            print "connection problems to the site, skipping"
            return None, 0, "", self.translated

        html_content = html_object.read()

        # unique header for the results, so only the results are getting displayed
        pattern = re.compile('[\t]+<td><a href="(http://mangafox.me/manga/[\w_]+/)"'
                             ' class="series_preview manga_open" rel="[\d]+">(.*)</a>')
        pattern_chapter_count = re.compile('<td>([\d]+)</td>')
        pattern_latest_chapter = re.compile('<a href=".*">(.*)</a> on .*')
        search_results = pattern.findall(html_content)

        if search_results:
            url, name = search_results[0]
            tmp_html_content = html_content.split(">%s</a>" % name)[1]
            chapter_count = pattern_chapter_count.findall(tmp_html_content)[1]
            latest_chapter = pattern_latest_chapter.findall(tmp_html_content)[0]
            return name, int(chapter_count), latest_chapter, self.translated
        else:
            return None, 0, "", self.translated

    def get_overview_page(self, manga):
        """
        create key for url based on title and 
        return the html content of the main page
        """
        self._current_manga = manga
        url = "http://www.mangafox.me/manga/%s" % self._get_title(manga).lower()
        url_content = self._session.open(url)
        if url_content:
            return url_content.read()
        else:
            return ''

    @staticmethod
    def get_latest_chapter(html_content):
        """
        check for latest chapter and return the title
        """
        if not html_content:
            return "404_site_content"
        pattern = re.compile('<a href="[^ ]+" .*class="tips">(.* [\d\.]{1,6})</a>')
        result = pattern.findall(html_content)
        if result:
            index = result[0]
            return str(index)
        else:
            return "Pattern misfit, please check the function get_latest_chapter"

    def fetch_new_chapters(self, current_chapter, html_content):
        """
        based on current chapter, fetch the newer chapters
        and return the title and a list of urls/paths of the images
        """
        if not self._current_manga:
            raise AttributeError('No manga defined,'
                                 ' please get the html content with the function get_overview to define the title')

        pattern = re.compile('<a href="([^ ]+)" .*class="tips">(.* [\d\.]{1,6})</a>')
        result = pattern.findall(html_content)

        if result:
            # local variable needed, since we access this class multiple times at the same time
            # but safe to clone it here, since iteration takes longer than regex check
            current_manga = self._current_manga
            sorted_list = list()
            for url, index in result:
                if current_chapter == index:
                    break
                sorted_list.append((url, index))
            sorted_list.reverse()

            for url, index in sorted_list:
                images = self._extract_images(url, index)
                if self._update_function:
                    self._update_function({index: (current_manga, index, images)})
                else:
                    self._logger.warn('No update function defined, please define an update function beforehand.')

    def _extract_images(self, url, path):
        images = list()
        mobile_view_url = url.replace('mangafox.me/manga/', 'm.mangafox.me/roll_manga/')
        html_content = self._session.open(mobile_view_url).read()
        pattern = re.compile('<img data-original="(.*)" class="reader-page" />')
        result = pattern.findall(html_content)
        if result:
            for page in result:
                page_index = result.index(page) + 1
                file_path = os.path.join(path, str(page_index))
                url = result[0]
                file_type = url.split('.')[-1]
                images.append((page, "%s.%s" % (file_path, file_type)))
        return images


__main_class__ = MangaFox

if __name__ == '__main__':
    dummy_session = urllib
    dummy_session.open = urllib.urlopen

    test_manga = "Monster Musume no Iru Nichijou"
    site = __main_class__()
    site.set_update_function(lambda x: pprint(x))
    site.set_session(dummy_session)
    print site.search_manga(test_manga)
    print site.get_latest_chapter(site.get_overview_page(test_manga))
    site.fetch_new_chapters("Monster Musume no Iru Nichijou 24", site.get_overview_page(test_manga))
