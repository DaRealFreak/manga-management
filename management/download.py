#!/usr/local/bin/python
# coding: utf-8

import inspect
import os
import re
import threading
import time

import databaseIO
import interface
import localization
import modules
import xml_parser

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


class DownloadDatabaseHandler(object):
    """
    class for the other modules, since downloader class is only for the download thread
    adding the data the Downloader class is using to the database
    """

    dbIO = databaseIO.DatabaseIO()

    def add_download(self, info):
        """
        add a download to the database, which is going to get fetched by DownloadThread

        :param info:
        :return:
        """
        if len(info) == 6:
            self.dbIO.add_download_queue(info)
        else:
            raise AttributeError(
                'download object has to have a length of 6, but has a length of "{0:d}"'.format(len(info))
            )


class Downloader(object):
    """
    external threaded class, managed with threading management class
    """

    modules = {}
    event = threading.Event()

    def __init__(self):
        """
        initializing function

        :return:
        """
        self.dbIO = databaseIO.DatabaseIO()
        self.xml_parser = xml_parser.XMLParser()
        if self.xml_parser.get_config("systemtray"):
            self.systemtray = interface.SystemTray()
        self.locale = localization.Locale()
        self.module_manager = modules.ModuleManager()
        if inspect.stack()[1][3] == 'run':
            self.check_download_queue()

    @staticmethod
    def validate_path(path):
        """
        validate the path according to windows rules

        :param path:
        :return:
        """
        path = re.sub('["/?*:<>\|]', '', os.path.normpath(path))
        for letter in path:
            if ord(letter) >= 128:
                path = path.replace(letter, "")
        return path

    def check_download_queue(self):
        """
        main function for the downloader, if any queue in currently in the database
        download it and remove it from the queue

        :return:
        """
        function_caller = inspect.stack()[1][3]
        waiting = False
        while True:
            if not self.event.is_set():
                self.event.set()

                queue = self.dbIO.get_download_queue()
                failed_downloads = []
                while queue and sorted(queue) != sorted(failed_downloads):
                    waiting = True
                    for row in queue:
                        idx, path, url, chapter_index, chapter_count, chapter_title, manga_title = row
                        if manga_title not in self.modules:
                            module_type = self.dbIO.get_manga(manga_title)["module"]
                            self.modules[manga_title] = self.module_manager.get_module_session(module_type)
                        download_session = self.modules[manga_title]
                        dl_path = self.validate_path(
                            os.path.join(self.xml_parser.get_download_path(), manga_title, path))
                        if not os.path.exists(os.path.dirname(dl_path)):
                            os.makedirs(os.path.dirname(dl_path))
                        # noinspection PyBroadException
                        try:
                            content = download_session.open(url).read()
                            if len(content) > 0:
                                open(dl_path, "wb").write(content)
                                self.dbIO.del_download(path, url)
                            else:
                                failed_downloads.append(row)
                            if chapter_index == chapter_count:
                                if self.xml_parser.get_config("systemtray"):
                                    self.systemtray.notify(self.locale.system_tray_update_notification_title,
                                                           self.locale.system_tray_update_notification_message %
                                                           chapter_title)
                                self.dbIO.add_update(manga_title, chapter_title)
                                self.dbIO.update_manga(manga_title, "chapter", chapter_title)
                        except Exception:
                            failed_downloads.append(row)
                    # refresh queue before sleeping in case we download while it adds new files to the queue
                    self.dbIO.commit()
                    queue = self.dbIO.get_download_queue()

                self.event.clear()

            if waiting > 0:
                waiting -= 1
                time.sleep(1)

            elif function_caller != '__init__' and waiting <= 0:
                break

            else:
                time.sleep(300)


if __name__ == '__main__':
    pass
