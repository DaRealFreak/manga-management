#!/usr/local/bin/python
# coding: utf-8

import sys

import localization
import xml_parser

if sys.platform in ('linux', 'linux2'):
    from management.cmdGui.linux import optiondialog, systemtray
elif sys.platform == "darwin":
    # Mac OS X uses linux commands for console, but a totally different systemtray
    from management.cmdGui.linux import optiondialog
    from management.cmdGui.mac import systemtray
elif sys.platform == "win32":
    from management.cmdGui.windows import optiondialog, systemtray

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


class SystemTray(object):
    """
    using the SystemTray as external object
    """

    __metaclass__ = localization.Singleton

    def __init__(self):
        """
         initializing function

         :return:
         """
        self.parser = xml_parser.XMLParser()
        self.systemtray = systemtray.SystemTrayWrapper()

    def __getattr__(self, name):
        """
        generic methods to call onto the browser object
        to guarantee full functionality of the specific session

        :type name: str
        :return:
        """
        if name in dir(self.systemtray):
            return getattr(self.systemtray, name)
        else:
            err = "'%s' object has no attribute '%s'" % (self.systemtray.__name__, name)
            raise AttributeError(err)


class CmdUserInterface(object):
    """
    initializes a graphical user interface for the command line
    """

    __metaclass__ = localization.Singleton

    def __init__(self):
        """
        initializing function

        :return:
        """
        self.parser = xml_parser.XMLParser()
        self.option_dialog = optiondialog.OptionDialog()

    def select_option(self, title="", available_options=None, timeout=0, timeout_function=None):
        """
        give the user a list of options where he can choose

        :param title:
        :param available_options:
        :param timeout:
        :param timeout_function:
        :return:
        """
        if not available_options:
            available_options = [
                {"message": "Exit",
                 "function": lambda: sys.exit(),
                 "args": ""
                 }
            ]

        self.option_dialog.request_option(title, available_options, timeout, timeout_function)


# noinspection PyUnresolvedReferences
user_interface = CmdUserInterface if xml_parser.XMLParser().get_config("cmdgui") else UserInterface

if __name__ == '__main__':
    import sys

    locale = localization.Locale()
    locale.load_locale("bin/language", "locale.txt")

    ui = CmdUserInterface()
    options = [
        {"message": "Hello World with no argument",
         "function": lambda: sys.stdout.write("no argument given"),
         "args": ()
         },
        {"message": "Hello World with 1 argument",
         "function": lambda x: sys.stdout.write(x),
         "args": "Hello!"
         },
        {"message": "Hello World with 2 arguments",
         "function": lambda x, y: sys.stdout.write("%s, %s" % (x, y)),
         "args": ("arg 1", "arg 2")
         }
    ]
    ui.select_option("Test option dialog:", options, timeout=0, timeout_function=None)
