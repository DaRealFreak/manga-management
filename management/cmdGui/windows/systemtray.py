#!/usr/local/bin/python
# coding: utf-8
import os
import time
import win32api

import win32con
import win32gui_struct

try:
    import winxpgui as win32gui
except ImportError:
    import win32gui

import management
from management import localization
from management import decorators
from management import xml_parser

__author__ = 'DaRealFreak <dasbaumchen@web.de>'
__credits__ = 'Mark Hammond\'s win32gui_taskbar.py and win32gui_menu.py demos from PyWin32'


# noinspection PyUnusedLocal,PyAttributeOutsideInit
class SystemTray(object):
    """
    windows system tray with icon option
    """

    QUIT = 'QUIT'
    SPECIAL_ACTIONS = [QUIT]

    FIRST_ID = 1023
    instance = None
    built = False

    # noinspection PyArgumentList
    def __new__(cls, *args, **kwargs):
        """
        create super instance from the current object

        :param cls:
        :param args:
        :param kwargs:
        :return:
        """
        if not cls.instance:
            cls.instance = super(SystemTray, cls).__new__(cls, *args, **kwargs)
        return cls.instance

    def __init__(self):
        """
        initializing function

        :return:
        """
        self.locale = localization.Locale()
        if __name__ != '__main__':
            parser = xml_parser.XMLParser()
            path = parser.get_localization_key("path")
            locale_file = parser.get_localization_key("file")
            self.locale.load_locale(os.path.abspath(path), locale_file)

    def start(self, icon, hover_text, menu_options, on_quit=None, default_menu_index=None, window_class_name=None):
        """
        setting all the neccessary variables and start the systemtray

        :param icon:
        :param hover_text:
        :param menu_options:
        :param on_quit:
        :param default_menu_index:
        :param window_class_name:
        :return:
        """
        self.icon = icon
        self.hover_text = hover_text
        self.on_quit = on_quit
        menu_options = menu_options + ((self.locale.system_tray_exit, None, self.QUIT),)
        self._next_action_id = self.FIRST_ID
        self.menu_actions_by_id = set()
        self.menu_options = self._add_ids_to_menu_options(list(menu_options))
        self.menu_actions_by_id = dict(self.menu_actions_by_id)
        del self._next_action_id
        self.default_menu_index = (default_menu_index or 0)
        self.window_class_name = window_class_name or "SystemTray"
        message_map = {win32gui.RegisterWindowMessage("TaskbarCreated"): self.refresh_icon,
                       win32con.WM_DESTROY: self.destroy,
                       win32con.WM_COMMAND: self.command,
                       win32con.WM_USER + 20: self.notify, }
        window_class = win32gui.WNDCLASS()
        hinst = window_class.hInstance = win32gui.GetModuleHandle(None)
        window_class.lpszClassName = self.window_class_name
        window_class.style = win32con.CS_VREDRAW | win32con.CS_HREDRAW
        window_class.hCursor = win32gui.LoadCursor(0, win32con.IDC_ARROW)
        window_class.hbrBackground = win32con.COLOR_WINDOW
        window_class.lpfnWndProc = message_map  # could also specify a wndproc.
        class_atom = win32gui.RegisterClass(window_class)
        style = win32con.WS_OVERLAPPED | win32con.WS_SYSMENU
        self.hwnd = win32gui.CreateWindow(class_atom,
                                          self.window_class_name,
                                          style,
                                          0,
                                          0,
                                          win32con.CW_USEDEFAULT,
                                          win32con.CW_USEDEFAULT,
                                          0,
                                          0,
                                          hinst,
                                          None)
        win32gui.UpdateWindow(self.hwnd)
        self.notify_id = None
        self.refresh_icon()
        self.built = True
        win32gui.PumpMessages()

    def _add_ids_to_menu_options(self, menu_options):
        """
        adding ids to the menu

        :param menu_options:
        :return:
        """
        result = []
        for menu_option in menu_options:
            option_text, option_icon, option_action = menu_option
            if callable(option_action) or option_action in self.SPECIAL_ACTIONS:
                # noinspection PyUnresolvedReferences
                self.menu_actions_by_id.add((self._next_action_id, option_action))
                result.append(menu_option + (self._next_action_id,))
            elif self.non_string_iterable(option_action):
                result.append((option_text,
                               option_icon,
                               self._add_ids_to_menu_options(option_action),
                               self._next_action_id))
            else:
                print 'Unknown item', option_text, option_icon, option_action
            self._next_action_id += 1
        return result

    def set_icon(self, icon_file):
        """
        set a new icon file

        :param icon_file:
        :return:
        """
        self.icon = icon_file
        self.refresh_icon()

    def refresh_icon(self):
        """
        refresh the set icon

        :return:
        """
        # Try and find a custom icon
        hinst = win32gui.GetModuleHandle(None)
        if self.icon and os.path.isfile(self.icon):
            if "__file__" in dir(self):
                self.icon = os.path.realpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), self.icon))
            icon_flags = win32con.LR_LOADFROMFILE | win32con.LR_DEFAULTSIZE
            self.hicon = win32gui.LoadImage(hinst,
                                            self.icon,
                                            win32con.IMAGE_ICON,
                                            0,
                                            0,
                                            icon_flags)
        else:
            print "Can't find icon file - using default."
            self.hicon = win32gui.LoadIcon(0, win32con.IDI_APPLICATION)

        if self.notify_id:
            message = win32gui.NIM_MODIFY
        else:
            message = win32gui.NIM_ADD
        self.notify_id = (self.hwnd,
                          0,
                          win32gui.NIF_ICON | win32gui.NIF_MESSAGE | win32gui.NIF_TIP,
                          win32con.WM_USER + 20,
                          self.hicon,
                          self.hover_text)
        win32gui.Shell_NotifyIcon(message, self.notify_id)

    def destroy(self, hwnd, msg, wparam, lparam):
        """
        destroying the systemtray

        :param hwnd:
        :param msg:
        :param wparam:
        :param lparam:
        :return:
        """
        if self.on_quit:
            self.on_quit(self)
        nid = (self.hwnd, 0)
        win32gui.Shell_NotifyIcon(win32gui.NIM_DELETE, nid)
        win32gui.PostQuitMessage(0)  # Terminate the app.

    def notify(self, hwnd, msg, wparam, lparam):
        """
        execute our python function call

        :param hwnd:
        :param msg:
        :param wparam:
        :param lparam:
        :return:
        """
        if lparam == win32con.WM_LBUTTONDBLCLK:
            self.execute_menu_option(self.default_menu_index + self.FIRST_ID)
        elif lparam == win32con.WM_RBUTTONUP:
            self.show_menu()
        elif lparam == win32con.WM_LBUTTONUP:
            pass
        return True

    def show_menu(self):
        """
        on leftclicking show the mnu

        :return:
        """
        menu = win32gui.CreatePopupMenu()
        self.create_menu(menu, self.menu_options)
        # win32gui.SetMenuDefaultItem(menu, 1000, 0)

        pos = win32gui.GetCursorPos()
        # See http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/menus_0hdi.asp
        win32gui.SetForegroundWindow(self.hwnd)
        win32gui.TrackPopupMenu(menu,
                                win32con.TPM_LEFTALIGN,
                                pos[0],
                                pos[1],
                                0,
                                self.hwnd,
                                None)
        win32gui.PostMessage(self.hwnd, win32con.WM_NULL, 0, 0)

    def create_menu(self, menu, menu_options):
        """
        create the menu

        :param menu:
        :param menu_options:
        :return:
        """
        for option_text, option_icon, option_action, option_id in menu_options[::-1]:
            if option_icon:
                option_icon = self.prep_menu_icon(option_icon)

            if option_id in self.menu_actions_by_id:
                item, extras = win32gui_struct.PackMENUITEMINFO(text=option_text,
                                                                hbmpItem=option_icon,
                                                                wID=option_id)
                win32gui.InsertMenuItem(menu, 0, 1, item)
            else:
                submenu = win32gui.CreatePopupMenu()
                self.create_menu(submenu, option_action)
                item, extras = win32gui_struct.PackMENUITEMINFO(text=option_text,
                                                                hbmpItem=option_icon,
                                                                hSubMenu=submenu)
                win32gui.InsertMenuItem(menu, 0, 1, item)

    def prep_menu_icon(self, icon):
        """
        load our icon and prepare it

        :param icon:
        :return:
        """
        # First load the icon.
        ico_x = win32api.GetSystemMetrics(win32con.SM_CXSMICON)
        ico_y = win32api.GetSystemMetrics(win32con.SM_CYSMICON)
        self.hicon = win32gui.LoadImage(0, icon, win32con.IMAGE_ICON, ico_x, ico_y, win32con.LR_LOADFROMFILE)

        hdc_bitmap = win32gui.CreateCompatibleDC(0)
        hdc_screen = win32gui.GetDC(0)
        hbm = win32gui.CreateCompatibleBitmap(hdc_screen, ico_x, ico_y)
        hbm_old = win32gui.SelectObject(hdc_bitmap, hbm)
        # Fill the background.
        brush = win32gui.GetSysColorBrush(win32con.COLOR_MENU)
        win32gui.FillRect(hdc_bitmap, (0, 0, 16, 16), brush)
        # unclear if brush needs to be feed.  Best clue I can find is:
        # "GetSysColorBrush returns a cached brush instead of allocating a new
        # one." - implies no DeleteObject
        # draw the icon
        win32gui.DrawIconEx(hdc_bitmap, 0, 0, self.hicon, ico_x, ico_y, 0, 0, win32con.DI_NORMAL)
        win32gui.SelectObject(hdc_bitmap, hbm_old)
        win32gui.DeleteDC(hdc_bitmap)

        return hbm

    def command(self, hwnd, msg, wparam, lparam):
        """
        execute option chosen from the menu

        :param hwnd:
        :param msg:
        :param wparam:
        :param lparam:
        :return:
        """
        menu_id = win32gui.LOWORD(wparam)
        self.execute_menu_option(menu_id)

    def execute_menu_option(self, menu_id):
        """
        execute selected option from the list

        :param menu_id:
        :return:
        """
        menu_action = self.menu_actions_by_id[menu_id]
        if menu_action == self.QUIT:
            win32gui.DestroyWindow(self.hwnd)
        else:
            menu_action(self)

    def show_notification(self, title, msg):
        """
        show the shell notification with the title and message

        :param title:
        :param msg:
        :return:
        """
        win32gui.Shell_NotifyIcon(win32gui.NIM_MODIFY,
                                  (self.hwnd, 0, win32gui.NIF_INFO, win32con.WM_USER + 20,
                                   self.hicon, "", msg, 200, title))

    @staticmethod
    def non_string_iterable(obj):
        """
        try to iter over the object
        :param obj:
        :return:
        """
        try:
            iter(obj)
        except TypeError:
            return False
        else:
            return not isinstance(obj, basestring)


class SystemTrayWrapper(object):
    """
    Wrapper to unify all system tray modules I'm using
    """

    def __init__(self):
        """
        initializing function

        :return:
        """
        self.system_tray = SystemTray()
        self.menu = None
        self.icon = None
        self.built = False

    def is_built(self):
        """
        check if the systemtray got constructed successfully
        
        :return:
        """
        return self.system_tray.built

    def set_menu(self, menu):
        """
        set the menu

        :param menu:
        :return:
        """
        self.menu = menu

    def set_icon(self, icon_file):
        """
        set an icon

        :param icon_file:
        :return:
        """
        self.icon = icon_file
        if self.menu:
            self.system_tray.set_icon(self.icon)

    def wait_for_construction(self):
        """
        simple waiting class while system tray gets constructed

        :return:
        """
        while not self.is_built():
            time.sleep(0.1)
        return True

    def notify(self, title, message):
        """
        show a system notification

        :param title:
        :param message:
        :return:
        """
        if self.build and not self.is_built():
            self.wait_for_construction()
        self.system_tray.show_notification(title, message)

    @decorators.RunAsynch("SystemTray")
    def build(self, on_quit=None):
        """
        starting function, using a new thread for further code functionality

        :param on_quit:
        :return:
        """
        if self.menu:
            self.built = True
            self.system_tray.start(self.icon, management.__name__, self.menu, on_quit=on_quit, default_menu_index=1)

    def exit(self):
        """
        closing function

        :return:
        """
        self.system_tray.on_quit(self.system_tray)


if __name__ == '__main__':
    import sys

    locale = localization.Locale()
    locale.load_locale("../../bin/language/", "locale.txt")
    sys_tray = SystemTrayWrapper()
    sys_tray.set_icon("../../bin/icon.ico")
    sys_tray.set_menu(
        (
            ("Send Test Message", None, lambda x: sys.stdout.write("Hello World")),
            (locale.system_tray_search, None, lambda x: 0),
            ('Send Test Notification', None, lambda x: x.show_notification(
                locale.system_tray_update_notification_title,
                locale.system_tray_update_notification_message % "Test Manga"))
        ),
    )
    sys_tray.build()
    sys_tray.notify("test", "test")
