#!/usr/local/bin/python
# coding: utf-8

import os
import time
from msvcrt import getch, kbhit, putch

from management import localization

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


class OptionDialog(object):
    def __init__(self):
        """
        initializing function

        :return:
        """
        self.locale = localization.Locale()

    @staticmethod
    def clear():
        """
        custom command for clearing the console on windows

        :return:
        """
        os.system("cls")

    def request_option(self, title, available_options, timeout, timeout_function):
        """
        display all available options and wait for the input

        :param title:
        :param available_options:
        :param timeout:
        :param timeout_function:
        :return:
        """
        self.clear()
        print title
        for option in available_options:
            message = option["message"]
            print "{0:d}: {1:s}".format(available_options.index(option) + 1, message)
        response = self.get_input(timeout, timeout_function)
        while response and response > len(available_options) and response > 0:
            print self.locale.option_invalid_answer
            response = self.get_input(timeout, timeout_function)

        if not response:
            timeout_function()
        elif hasattr(response, '__call__'):
            response()
        else:
            option = available_options[response - 1]
            if not isinstance(option["args"], (tuple, list, dict)):
                option["function"](option["args"])
            else:
                option["function"](*option["args"])

    def get_input(self, timeout, timeout_function):
        """
        wait for the user input or the timeout

        :param timeout:
        :param timeout_function:
        :return:
        """
        if not timeout:
            response = raw_input(self.locale.option_select_option + '\n')
            while not response.isdigit():
                print self.locale.option_invalid_digit
                response = raw_input()
            return int(response)

        answer_string = ""
        end_time = time.time() + timeout

        print self.locale.option_select_option
        while True:
            while time.time() < end_time:
                if kbhit():
                    break
                else:
                    time.sleep(0.05)
            # here comes our timeout check, called if end_time > our actual timestamp
            else:
                print self.locale.option_timeout
                # if we defined a timeout function, return it, else call the same process again
                if timeout_function:
                    return timeout_function
                return None

            char = getch()
            if char == '\r':
                break
            answer_string += char
            putch(char)

        if answer_string.isdigit():
            return int(answer_string)
        else:
            print self.locale.option_invalid_digit
            return self.get_input(timeout, timeout_function)


if __name__ == '__main__':
    import sys

    locale = localization.Locale()
    locale.load_locale("../../bin/language/", "locale.txt")

    dialog = OptionDialog()
    options = [
        {"message": "Hello World",
         "function": lambda x: sys.stdout.write(x),
         "args": "Hello!"
         }
    ]
    dialog.request_option("Test option dialog:", options, timeout=0, timeout_function=None)
