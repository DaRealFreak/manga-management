#!/usr/local/bin/python
# coding: utf-8

import difflib
import operator
import re

from management.modules import session
# external dependencies
import numpy as np

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


# noinspection PyShadowingNames
class BakaUpdates(object):
    """
    module for extracting alternative language titles for titles from mangaupdates.com
    """

    main_url = "https://www.mangaupdates.com/"

    def __init__(self):
        """
        initializing function

        :return:
        """
        self.browser = session.Session().get_session("mechanize")

    def search_titles(self, title):
        """
        main function for extracting alternate titles

        :param title:
        :return:
        """
        self.browser.open(self.main_url)
        self.browser.select_form(nr=2)
        self.browser.form["search"] = title
        response = self.browser.submit()
        res = response.read()
        results = re.findall("<a href='([^\s]+)' title='Series Info'>(.*)</a>[\*]?</td>", res)
        open("test.html", "w").write(res)
        matches = []
        used_urls = []
        for url, res_title in results:
            if url in used_urls:
                continue
            used_urls.append(url)
            match = difflib.SequenceMatcher(None, res_title.lower(), title.lower()).ratio()
            matches.append((res_title, url, match))
        matches.sort(key=operator.itemgetter(2), reverse=True)
        if matches:
            match_url = matches[0][1]
            return self.open_title(match_url)
        return []

    @staticmethod
    def remove_suffix(title):
        """
        if a Novel is the best match, we don't want this in our title tag

        :param title:
        :return:
        """
        return title

    def open_title(self, url):
        """
        parse the page content of the best matched title

        :param url:
        :return:
        """
        url_content = self.browser.open(url).read()
        public_title = re.findall('<span class="releasestitle tabletitle">([^<]+)</span>',
                                  url_content)[0].rsplit("(Novel", 1)[0].strip()
        res = re.findall('<b>Associated Names</b></div>\n<div class="sContent" >(.*)', url_content)
        if res:
            titles = res[0].split('<br />')
            titles = [self.remove_suffix(t) for t in titles]
            jp_raw_titles = self.extract_japanese_titles(titles)
            kr_raw_titles = self.extract_korean_titles(titles)
            en_titles = [t for t in titles if not bool(re.search('&#\d{4,6};', t)) and len(t) > 0]
            jp_titles = [self.assemble_unicode_title(title) for title in jp_raw_titles]
            kr_titles = [self.assemble_unicode_title(title) for title in kr_raw_titles]

            if public_title not in en_titles:
                en_titles.insert(0, public_title)

            return {
                "en": en_titles,
                "jp": jp_titles,
                "kr": kr_titles
            }
        else:
            return {
                "en": [public_title],
                "jp": [],
                "kr": []
            }

    def extract_japanese_titles(self, titles):
        """
        extract the japanese titles from the page content based on the used characters

        :param titles:
        :return:
        """
        return [title for title in titles if
                bool(re.search('&#\d{4,6};', title)) and self.determine_japanese_title(title)]

    def extract_korean_titles(self, titles):
        """
        extract the korean titles from the page content based on the used characters

        :param titles:
        :return:
        """
        return [title for title in titles if
                bool(re.search('&#\d{4,6};', title)) and self.determine_korean_title(title)]

    @staticmethod
    def determine_japanese_title(title):
        """
        determine based on unicode elements, if the title is japanese.
        JAPANESE_PUNCTUATION=(0x3000, 0x3F)
        JAPANESE_HIRAGANA=(0x3040, 0x5f)
        JAPANESE_KATAKANA=(0x30A0, 0x5f)
        JAPANESE_ROMAN_HALF_WIDTH_KATAKANA=(0xFF00, 0xEF)
        JAPANESE_KANJI=(0x4e00, 0x51AF)
        JAPANESE_KANJI_RARE=(0x3400, 0x19BF)

        :param title:
        :return:
        """
        lows = np.array([0x3000, 0x3040, 0x30a0, 0xff00, 0x4e00, 0x3400])  # the lower bounds
        ups = np.array([0x303f, 0x309f, 0x30ff, 0xffef, 0x51AF, 0x4dbf])  # the upper bounds
        unichars = re.findall('&#([\d]{4,6});', title)
        # any thanks to shared characters with chinese
        return any([np.any((lows <= int(unichar)) & (int(unichar) <= ups)) for unichar in unichars])

    @staticmethod
    def determine_korean_title(title):
        """
        determine based on unicode elements, if the title is korean.
        Hangul Syllables (AC00-D7A3) which corresponds to (가-힣)
        Hangul Jamo (1100–11FF)
        Hangul Compatibility Jamo (3130-318F)
        Hangul Jamo Extended-A (A960-A97F)
        Hangul Jamo Extended-B (D7B0-D7FF)

        :param title:
        :return:
        """
        lows = np.array([0xAC00, 0x1100, 0x3130, 0xA960, 0xD7B0])  # the lower bounds
        ups = np.array([0xD7A3, 0x11FF, 0x318F, 0xA97F, 0xD7FF])  # the upper bounds
        unichars = re.findall('&#([\d]{4,6});', title)
        # not sure but all titles I found so far have a clear character set, not shared
        return all([np.any((lows <= int(unichar)) & (int(unichar) <= ups)) for unichar in unichars])

    @staticmethod
    def assemble_unicode_title(title):
        """
        replace the stand-in characters from our title with the real characters

        :param title:
        :return:
        """
        unichars = re.findall('&#([\d]{4,6});', title)
        for unichar in unichars:
            title = title.replace(u'&#{0:s};'.format(unichar), unicode(unichr(int(unichar))))
        return title


instance = BakaUpdates

if __name__ == '__main__':
    bu = BakaUpdates()
    titles = bu.search_titles("Tate no Yuusha no Nariagari")
    for language in titles:
        for title in titles[language]:
            try:
                print "[{0:s}]: {1:s}".format(language, title)
            except UnicodeEncodeError:
                print "[{0:s}]: lenghth: {1:d}".format(language, len(title))
