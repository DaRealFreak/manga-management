#!/usr/local/bin/python
# coding: utf-8

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


class Taadd(object):
    update_instance = None
    session = None

    def set_update_function(self, update_instance):
        """
        sets the update function

        :param update_instance:
        :return:
        """
        self.update_instance = update_instance

    def set_session(self, session):
        """
        sets the session

        :param session:
        :return:
        """
        self.session = session

    def search(self, title):
        """
        get all necessary informations from module based on title

        :param title: str
        :return title: string
        :return chapter: int
        :return language: string
        :return path: string
        :return url: string
        :return module: string
        :return active: bool
        """
        pass

    def get_latest_chapter(self, url):
        """
        get latest chapter based on saved url

        :param url:
        :return chapter: string
        """
        pass

    def fetch_new_chapters(self, current_chapter):
        """
        fetch all new chapters since the current_chapter and call the defined update function

        :param current_chapter:
        :return:
        """
        pass


instance = Taadd

if __name__ == '__main__':
    import urllib
    import pprint

    dummy_session = urllib
    dummy_session.open = urllib.urlopen
    test_manga = "Test"
    test_chapter = "Test 1"

    manga_object = instance()
    manga_object.set_session(dummy_session)
    manga_object.set_update_function(lambda x: pprint.pprint(x))
    manga_object.search(test_manga)
    manga_object.get_latest_chapter(test_manga)
    manga_object.fetch_new_chapters(test_chapter)
