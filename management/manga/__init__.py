#!/usr/local/bin/python
# coding: utf-8

from management.manga import mangafox
from management.manga import mangahere
from management.manga import mangareader
from management.manga import taadd

__all__ = ['mangafox', 'mangahere', 'mangareader', 'taadd']
