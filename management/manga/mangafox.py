#!/usr/local/bin/python
# coding: utf-8

import Queue
import difflib
import operator
import os
import re
import threading
import time

from bs4 import BeautifulSoup

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


class ProcessingQueue(threading.Thread):
    """
    Queue for smoother processing while script is running
    """

    def __init__(self, queue, session, manga, update_instance):
        """
        initializing function

        :param queue:
        :param session:
        :param update_instance:
        :return:
        """
        super(ProcessingQueue, self).__init__(name="MangafoxProcessingQueue")
        self.queue = queue
        self.session = session
        self.manga = manga
        self.update_instance = update_instance

    def run(self):
        """
        main function of our worker thread

        :return:
        """
        while True:
            # grabs host from queue
            path_title, current_url = self.queue.get()
            if (path_title, current_url) == (None, None):
                # break out of the while True loop on receiving the signal objects
                break
            mobile_url = current_url.replace("http://mangafox.me/manga/", "http://m.mangafox.me/roll_manga/")
            content = self.session.open(mobile_url).read()
            if 'licensed and not available' in content:
                self.process_pages(path_title, current_url)
            else:
                self.process_mobile_view(path_title, content)

            # signals to queue job is done
            self.queue.task_done()

    def process_pages(self, title, current_url):
        """
        processing the standard view(for licensed mangas, mobile view is way faster)

        :param title:
        :param current_url:
        :return:
        """

        def extract_image(current_content):
            current_soup = BeautifulSoup(current_content, "lxml")
            res = current_soup.find("img", {"id": "image"})
            if res:
                return res["src"]
            else:
                return None

        pages = []
        content = self.session.open(current_url).read()
        soup = BeautifulSoup(content, "lxml")
        select = soup.find("select", {"class": "m"})
        options = select.find_all("option")
        for option in options[1:]:
            if option.text.isdigit():
                pages.append(option.text)

        image = extract_image(content)
        if image:
            file_type = image.rsplit(".")[-1]
            self.update_instance((os.path.join(title, "1.{0:s}".format(file_type)), image, 0, len(pages) - 1, title,
                                  self.manga))
        for page in pages:
            current_url = re.sub('[\d]+.html', u'{0:s}.html'.format(page), current_url)
            image = extract_image(self.session.open(current_url).read())
            if image:
                file_type = image.rsplit(".")[-1]
                self.update_instance((os.path.join(title, "{0:s}.{1:s}".format(page, file_type)), image,
                                      pages.index(page), len(pages) - 1, title, self.manga))

    def process_mobile_view(self, title, content):
        """
        processing the mobile view(faster compared to page view, using this if possible)

        :param title:
        :param content:
        :return:
        """
        soup = BeautifulSoup(content, "lxml")
        images = soup.find_all("img", {"class": "reader-page"})
        for image in images:
            page = images.index(image) + 1
            image_url = image["data-original"]
            file_type = image_url.rsplit(".")[-1]
            self.update_instance((os.path.join(title, "{0:d}.{1:s}".format(page, file_type)), image_url,
                                  images.index(image), len(images) - 1, title, self.manga))


class Mangafox(object):
    update_instance = None
    session = None
    language = "en"

    @staticmethod
    def parse_title(title):
        """
        parse title in url form

        :param title:
        :return:
        """
        title_pattern = re.compile("[^\w]")
        title = title_pattern.sub('_', title)
        title = title.strip("_")
        while title.count('__'):
            title = title.replace('__', '_')
        return title

    @staticmethod
    def parse_results(results):
        """
        parse the results to readable and useable dict entries

        :param results:
        :return:
        """
        informations = {}
        for result in results:
            result = list(result.values())
            name, _, views, chapters, update = result
            name, current_url = name[0].split("\n")[0], name[1]
            informations[name] = {}
            informations[name]["name"] = name
            informations[name]["url"] = current_url
            informations[name]["chapter_count"] = int(chapters)
            informations[name]["update"] = update[0].strip("\n\t\r")
            informations[name]["update_url"] = update[1]
        return informations

    @staticmethod
    def find_best_match(title, keys):
        """
        find the closest levenshtein distance

        :param title:
        :param keys:
        :return:
        """
        matches = []
        for key in keys:
            match = difflib.SequenceMatcher(None, key.lower(), title.lower()).ratio()
            matches.append((key, match))
        matches.sort(key=operator.itemgetter(1), reverse=True)
        return matches[0][0]

    def set_update_function(self, update_instance):
        """
        sets the update function

        :param update_instance:
        :return:
        """
        self.update_instance = update_instance

    def set_session(self, session):
        """
        sets the session

        :param session:
        :return:
        """
        self.session = session

    def search(self, title):
        """
        get all necessary informations from module based on title

        :param title: string
        :return informations: (string, string, int, string, string, string, bool)
        """
        # works only with ALL parameters, all on standard, only keyword search
        # on searching with form, we have to sleep 5 seconds, so we choose this type of action
        search_url = (u'http://mangafox.me/search.php?name_method=cw&name={0:s}&type=&author_method=cw&author='
                      u'&artist_method=cw&artist=&genres[Action]=0&genres[Adult]=0&genres[Adventure]=0&genres[Comedy]=0'
                      u'&genres[Doujinshi]=0&genres[Drama]=0&genres[Ecchi]=0&genres[Fantasy]=0&genres[Gender+Bender]=0'
                      u'&genres[Harem]=0&genres[Historical]=0&genres[Horror]=0&genres[Josei]=0&genres[Martial+Arts]=0'
                      u'&genres[Mature]=0&genres[Mecha]=0&genres[Mystery]=0&genres[One+Shot]=0&genres[Psychological]=0'
                      u'&genres[Romance]=0&genres[School+Life]=0&genres[Sci-fi]=0&genres[Seinen]=0&genres[Shoujo]=0'
                      u'&genres[Shoujo+Ai]=0&genres[Shounen]=0&genres[Shounen+Ai]=0&genres[Slice+of+Life]=0'
                      u'&genres[Smut]=0&genres[Sports]=0&genres[Supernatural]=0&genres[Tragedy]=0&genres[Webtoons]=0'
                      u'&genres[Yaoi]=0&genres[Yuri]=0&released_method=eq&released=&rating_method=eq&rating='
                      u'&is_completed=&advopts=1'
                      .format(self.parse_title(title)))

        """
        formcount = 0
        for frm in self.session.forms():
            if str(frm.attrs["id"]) == "searchform":
                break
            formcount += 1
        self.session.select_form(nr=formcount)
        """

        content = self.session.open(search_url).read()
        if 'search again within 5 seconds' in content:
            time.sleep(5.5)
            return self.search(title)

        soup = BeautifulSoup(content, "lxml")
        table = soup.find("table", {"id": "listing"})
        rows = table.find_all("tr")

        # name, views, chapters, update = information_rows
        results = []
        if len(rows) > 1:
            for row in rows[1:]:
                results.append({})
                information_rows = row.find_all("td")
                i = 0
                for information_row in information_rows:
                    a_tag = information_row.find('a', href=True)
                    if a_tag:
                        current_url = a_tag['href']
                        results[rows[1:].index(row)][i] = (information_row.text, current_url)
                    else:
                        results[rows[1:].index(row)][i] = information_row.text
                    i += 1
        else:
            return [''] * 7

        results = self.parse_results(results)
        key = self.find_best_match(title, results.keys())

        latest_chapter = self.get_latest_chapter(results[key]["url"])
        latest_chapter = latest_chapter[0].strip()

        return (
            results[key]["name"],
            results[key]["url"],
            results[key]["chapter_count"],
            latest_chapter,
            self.language,
            "mangafox",
            True
        )

    def get_chapters(self, current_url):
        """
        get all chapters with all informations available on the overview page

        :param current_url:
        :return:
        """
        chapters = []
        content = self.session.open(current_url).read()
        soup = BeautifulSoup(content, "lxml")
        sub_parts = soup.find_all("ul", {"class": "chlist"})
        for sub_part in sub_parts:
            chapter_soups = sub_part.find_all("div")
            for chapter_soup in chapter_soups:
                date = chapter_soup.find("span", {"class": "date"}).text
                chapter = chapter_soup.find("a", {"class": "tips"})
                chapter_url = chapter["href"]
                chapter_name = chapter.text
                title = chapter_soup.find("span", {"class": "title nowrap"})
                if title:
                    title = title.text
                else:
                    title = ""
                if title == "Read Online":
                    title = ""
                title = title.encode("utf-8")
                chapter = "{0:s} {1:s}".format(chapter_name, title).strip()
                chapters.append((chapter, chapter_name, chapter_url, date))
        return chapters

    def get_latest_chapter(self, current_url):
        """
        get latest chapter based on saved url

        :param current_url:
        :return chapter: string
        """
        chapters = self.get_chapters(current_url)
        if chapters:
            return chapters[0]
        else:
            return [""] * 4

    def fetch_new_chapters(self, current_url, manga_title, current_chapter):
        """
        fetch all new chapters since the current_chapter and call the defined update function

        :param current_url:
        :param manga_title:
        :param current_chapter:
        :return:
        """
        queue = Queue.Queue()
        t = ProcessingQueue(queue, self.session, manga_title, self.update_instance)
        t.setDaemon(True)
        t.start()

        chapters = self.get_chapters(current_url)
        sorted_chapters = []
        for title, path_title, current_url, date in chapters:
            if path_title == current_chapter.encode('utf-8'):
                break
            sorted_chapters.append((path_title, current_url))

        while sorted_chapters:
            queue.put(sorted_chapters.pop())

        # wait on the queue until everything has been processed
        queue.join()
        # put our signal into the queue to break out of the while True loop and terminate the thread
        queue.put((None, None))


setup = (Mangafox, "mechanize")

if __name__ == '__main__':
    import urllib
    import pprint
    import gzip
    import cStringIO


    def dummy_open(current_url):
        text = urllib.urlopen(current_url).read()
        magic = bytearray(text)[:2]
        # check for gzip encoding
        if magic == b"\x1f\x8b":
            buf = cStringIO.StringIO(text)
            f = gzip.GzipFile(fileobj=buf)
            text = f.read()
        return cStringIO.StringIO(text)


    dummy_session = urllib
    dummy_session.open = dummy_open

    # Maken-Ki is licensed, processing standard page view
    test_manga = "Maken-Ki!"
    test_chapter = ""

    manga_object = setup[0]()
    manga_object.set_session(dummy_session)
    manga_object.set_update_function(lambda x: pprint.pprint(x))
    infos = manga_object.search(test_manga)
    print infos
    url = infos[1]
    manga_object.get_latest_chapter(url)
    manga_object.fetch_new_chapters(url, test_chapter, test_manga)
