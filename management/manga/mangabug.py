#!/usr/local/bin/python
# coding: utf-8

import Queue
import difflib
import operator
import os
import re
import threading

from bs4 import BeautifulSoup

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


class ProcessingQueue(threading.Thread):
    """
    Queue for smoother processing while script is running
    """

    def __init__(self, queue, session, manga, update_instance):
        """
        initializing function

        :param queue:
        :param session:
        :param update_instance:
        :return:
        """
        super(ProcessingQueue, self).__init__(name="MangabugProcessingQueue")
        self.queue = queue
        self.session = session
        self.manga = manga
        self.update_instance = update_instance

    def run(self):
        """
        main function of our worker thread

        :return:
        """
        while True:
            # grabs host from queue
            path_title, current_url = self.queue.get()
            if (path_title, current_url) == (None, None):
                # break out of the while True loop on receiving the signal objects
                break
            self.process_pages(path_title, current_url)

            # signals to queue job is done
            self.queue.task_done()

    def process_pages(self, title, current_url):
        """
        processing the standard view(for licensed mangas, mobile view is way faster)

        :param title:
        :param current_url:
        :return:
        """

        def extract_image(current_content):
            current_soup = BeautifulSoup(current_content, "lxml")
            res = current_soup.find("img", {"id": "image"})
            if res:
                return res["src"]
            else:
                return None

        pages = []
        content = self.session.open(current_url).read()
        soup = BeautifulSoup(content, "lxml")
        select = soup.find("select", {"class": "m"})
        options = select.find_all("option")
        for option in options[1:]:
            if option.text.isdigit():
                pages.append(option.text)

        image = extract_image(content)
        if image:
            file_type = image.rsplit(".")[-1]
            self.update_instance((os.path.join(title, "1.{0:s}".format(file_type)), image, 0, len(pages) - 1, title,
                                  self.manga))
        for page in pages:
            current_url = re.sub('[\d]+.html', u'{0:s}.html'.format(page), current_url)
            image = extract_image(self.session.open(current_url).read())
            if image:
                file_type = image.rsplit(".")[-1]
                self.update_instance((os.path.join(title, "{0:s}.{1:s}".format(page, file_type)), image,
                                      pages.index(page), len(pages) - 1, title, self.manga))


class Mangabug(object):
    update_instance = None
    session = None
    language = "en"

    @staticmethod
    def parse_title(title):
        """
        parse title in url form

        :param title:
        :return:
        """
        title_pattern = re.compile("[^\w]")
        title = title_pattern.sub('_', title)
        title = title.strip("_")
        while title.count('__'):
            title = title.replace('__', '_')
        return title

    @staticmethod
    def find_best_match(title, keys):
        """
        find the closest levenshtein distance

        :param title:
        :param keys:
        :return:
        """
        matches = []
        for key in keys:
            match = difflib.SequenceMatcher(None, key.lower(), title.lower()).ratio()
            matches.append((key, match))
        matches.sort(key=operator.itemgetter(1), reverse=True)
        return matches[0][0]

    def set_update_function(self, update_instance):
        """
        sets the update function

        :param update_instance:
        :return:
        """
        self.update_instance = update_instance

    def set_session(self, session):
        """
        sets the session

        :param session:
        :return:
        """
        self.session = session

    def search(self, title):
        """
        get all necessary informations from module based on title

        :param title: string
        :return informations: (string, string, int, string, string, string, bool)
        """
        self.session.open("http://www.mangabug.com")
        self.session.select_form(nr=0)
        self.session["txt_wpm_wgt_mng_sch_nme"] = title
        content = self.session.submit().read()
        soup = BeautifulSoup(content, "lxml")
        tables = soup.find_all("div", {"class": "nde"})
        results = {}
        for table in tables:
            a_tag = table.find_all('a', href=True)
            if len(a_tag) == 2:
                results[a_tag[1].text] = {}
                results[a_tag[1].text]["url"] = a_tag[1]['href']
        if not len(results):
            return [''] * 7

        best_match = self.find_best_match(title, results.keys())
        latest_chapter = self.get_latest_chapter(results[best_match]["url"])
        latest_chapter = latest_chapter[0].strip()

        return (
            results[best_match]["name"],
            results[best_match]["url"],
            results[best_match]["chapter_count"],
            latest_chapter,
            self.language,
            "mangabug",
            True
        )

    def get_chapters(self, current_url):
        """
        get all chapters with all informations available on the overview page

        :param current_url:
        :return:
        """
        chapters = []
        content = self.session.open(current_url).read()
        soup = BeautifulSoup(content, "lxml")
        sub_parts = soup.find_all("ul", {"class": "chlist"})
        for sub_part in sub_parts:
            chapter_soups = sub_part.find_all("div")
            for chapter_soup in chapter_soups:
                date = chapter_soup.find("span", {"class": "date"}).text
                chapter = chapter_soup.find("a", {"class": "tips"})
                chapter_url = chapter["href"]
                chapter_name = chapter.text
                title = chapter_soup.find("span", {"class": "title nowrap"})
                if title:
                    title = title.text
                else:
                    title = ""
                if title == "Read Online":
                    title = ""
                title = title.encode("utf-8")
                chapter = "{0:s} {1:s}".format(chapter_name, title).strip()
                chapters.append((chapter, chapter_name, chapter_url, date))
        return chapters

    def get_latest_chapter(self, current_url):
        """
        get latest chapter based on saved url

        :param current_url:
        :return chapter: string
        """
        chapters = self.get_chapters(current_url)
        if chapters:
            return chapters[0]
        else:
            return [""] * 4

    def fetch_new_chapters(self, current_url, manga_title, current_chapter):
        """
        fetch all new chapters since the current_chapter and call the defined update function

        :param current_url:
        :param manga_title:
        :param current_chapter:
        :return:
        """
        queue = Queue.Queue()
        t = ProcessingQueue(queue, self.session, manga_title, self.update_instance)
        t.setDaemon(True)
        t.start()

        chapters = self.get_chapters(current_url)
        sorted_chapters = []
        for title, path_title, current_url, date in chapters:
            if path_title == current_chapter.encode('utf-8'):
                break
            sorted_chapters.append((path_title, current_url))

        while sorted_chapters:
            queue.put(sorted_chapters.pop())

        # wait on the queue until everything has been processed
        queue.join()
        # put our signal into the queue to break out of the while True loop and terminate the thread
        queue.put((None, None))


setup = (Mangabug, "mechanize")

if __name__ == '__main__':
    import pprint
    import cookielib
    import mechanize
    import warnings

    cj = cookielib.LWPCookieJar()
    dummy_session = mechanize.Browser()
    dummy_session.set_cookiejar(cj)
    dummy_session.set_handle_equiv(True)
    warnings.filterwarnings('ignore', 'gzip transfer encoding is experimental!', )
    dummy_session.set_handle_gzip(True)
    dummy_session.set_handle_redirect(True)
    dummy_session.set_handle_referer(True)
    dummy_session.set_handle_robots(False)
    # noinspection PyProtectedMember
    dummy_session.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)
    urlagent = 'Mozilla/5.3 (Windows; U; Windows NT 5.1; en-US; rv:6.8.0.7) Gecko/2065092957 Firefox/6.9.0.2'
    dummy_session.addheaders = [("User-agent", urlagent)]

    test_manga = "Douluo Dalu"
    test_chapter = ""

    manga_object = setup[0]()
    manga_object.set_session(dummy_session)
    manga_object.set_update_function(lambda x: pprint.pprint(x))
    infos = manga_object.search(test_manga)
    print infos
# url = infos[1]
#    manga_object.get_latest_chapter(url)
#    manga_object.fetch_new_chapters(url, test_chapter, test_manga)
