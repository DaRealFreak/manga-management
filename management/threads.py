#!/usr/local/bin/python
# coding: utf-8

import threading
import time

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


# noinspection PyShadowingNames
class ThreadingManagement(object):
    CONTROL_TIME = 60
    threads = {}

    def add_thread(self, name, function, args=None, kwargs=None):
        """
        add a thread with his corresponding function to the list and start it if not existent

        :param name:
        :param function:
        :param args:
        :param kwargs:
        :return:
        """
        # to prevent mutable objects we have to define them here based on the None value
        if not args:
            args = list()
        if not kwargs:
            kwargs = dict()

        if name not in self.threads:
            self.threads[name] = {
                "function": function,
                "args": args,
                "kwargs": kwargs
            }

        # instead of checking and manually starting the thread, we just use the standard control function
        self.control_threads()

    @staticmethod
    def run_once(function, args=None, kwargs=None, delay=None):
        """
        run a function once in an unnamed thread

        :param function:
        :param args:
        :param kwargs:
        :param delay:
        :return:
        """
        if not args:
            args = []
        if not kwargs:
            kwargs = {}
        if delay:
            threading.Timer(float(delay), function, args=args, kwargs=kwargs).start()
        else:
            threading.Thread(target=function, args=args, kwargs=kwargs).start()

    def get_threads(self):
        """
        returns the controlled threads

        :return:
        """
        return self.threads.keys()

    def force_kill_thread(self, name):
        """
        kill a thread based on name if the thread is registered

        :param name:
        :return:
        """
        if name not in self.threads:
            raise MemoryError("Out of scope, thread not registered")

        if name == "MainThread":
            raise MemoryError("Can't kill main thread")

        # generally bad code style, but since the threads are sleeping for multiple minutes
        # I force kill them here on command
        for thread in threading.enumerate():
            if thread.getName() == name:
                # noinspection PyProtectedMember
                thread._Thread__stop()

    def exit(self):
        """
        kill all threads for fast exit

        :return:
        """
        for thread in self.get_threads():
            self.force_kill_thread(thread)

    def control_threads(self):
        """
        controlling all running threads for the specified threads

        :return:
        """
        check_list = self.threads.keys()

        for thread in threading.enumerate():
            name = thread.getName()
            if name in check_list:
                check_list.remove(name)

        for thread_name in check_list:
            # print 'Thread "%s" died, restarting...' % thread_name
            function = self.threads[thread_name]["function"]
            args = self.threads[thread_name]["args"]
            kwargs = self.threads[thread_name]["kwargs"]
            threading.Thread(target=function, name=thread_name, args=args, kwargs=kwargs).start()

    def start_control_thread(self):
        """
        start the control function in a new thread to be independent from the main thread

        :return:
        """
        self.add_thread("ControlThread", self.start_control)

    def start_control(self):
        """
        start controlling the threads

        :return:
        """
        while True:
            self.control_threads()
            time.sleep(self.CONTROL_TIME)


if __name__ == '__main__':
    thread_management = ThreadingManagement()


    def error_function():
        while True:
            if int(time.time()) % 15 == 0:
                raise AssertionError("modulo 15 reached, raising error")


    def normal_function():
        while True:
            pass


    thread_management.CONTROL_TIME = 10
    thread_management.add_thread("ErrorThread", error_function)
    thread_management.add_thread("NormalThread", normal_function)

    thread_management.start_control_thread()

    rounds = 0
    while True:
        if rounds >= 25:
            print "25 test rounds reached, killing threads..."
            break

        rounds += 1
        time.sleep(1)

    for thread in thread_management.get_threads():
        print 'killing "%s"' % thread
        thread_management.force_kill_thread(thread)
