#!/usr/local/bin/python
# coding: utf-8

import os
import re
import threading

from bs4 import BeautifulSoup

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


class Configuration:
    """
    configuration class, returning available options
    """

    @staticmethod
    def get_available_settings():
        return ["startup", "autoupdate", "systemtray", "cmdgui"]

    def __init__(self):
        pass


# noinspection PyShadowingNames,PyShadowingBuiltins
class XMLParser(object):
    """
    usage for binding the mangas to their specific modules
    also usage for common configurations like database path, windows startup,...
    """

    instance = None
    pretty_mod = False
    lock = threading.Lock()

    xml = None
    file_name = os.path.join(os.path.dirname(__file__), os.pardir, "settings.xml")

    def __init__(self):
        """
        initializing function

        :return:
        """

        def prettify(self, encoding=None, formatter="minimal", indent_width=4):
            return r.sub(r'\1' * indent_width, orig_prettify(self, encoding, formatter))

        if not self.pretty_mod:
            orig_prettify = BeautifulSoup.prettify
            r = re.compile(r'^(\s*)', re.MULTILINE)

            BeautifulSoup.prettify = prettify
            self.pretty_mod = True

        self.load_xml()

    def __del__(self):
        """
        del function

        :return:
        """
        self.save_xml()
        self.xml = None

    # noinspection PyArgumentList
    def __new__(cls, *args, **kwargs):
        """
        create super instance from the current object

        :param cls:
        :param args:
        :param kwargs:
        :return:
        """
        if not cls.instance:
            cls.instance = super(XMLParser, cls).__new__(cls, *args, **kwargs)
        return cls.instance

    def get_database_path(self):
        """
        returns the database path

        :return:
        """
        if not self.xml:
            raise AttributeError("XML is not defined or empty.")
        return self.xml.configuration.database["path"]

    def set_database_path(self, path):
        """
        sets the database path

        :param path:
        :return:
        """
        if not self.xml:
            raise AttributeError("XML is not defined or empty.")
        if os.access(os.path.dirname(path), os.W_OK):
            self.xml.configuration.database["path"] = path
        else:
            raise UserWarning("selected path is not access able")
        self.save_xml()

    def get_download_path(self):
        """
        returns the base download path

        :return:
        """
        if not self.xml:
            raise AttributeError("XML is not defined or empty.")
        return self.xml.configuration.download["directory"]

    def set_download_path(self, path):
        """
        sets the base download path

        :param path:
        :return:
        """
        if not self.xml:
            raise AttributeError("XML is not defined or empty.")
        if os.access(os.path.dirname(path), os.W_OK):
            self.xml.configuration.download["directory"] = path
        else:
            raise UserWarning("selected path is not access able")
        self.save_xml()

    def get_config(self, key):
        """
        returns a config setting based on the key

        :param key:
        :return:
        """
        if not self.xml:
            raise AttributeError("XML is not defined or empty.")

        settings = {
            "startup": self.xml.configuration.startup,
            "autoupdate": self.xml.configuration.autoupdate,
            "systemtray": self.xml.configuration.systemtray,
            "cmdgui": self.xml.configuration.cmdgui,
        }
        if key not in settings:
            raise AttributeError("Key not existent in configurations")

        if not settings[key].has_attr("bool"):
            if not self.validate_key(u"configuration.{0:s}".format(key), key):
                raise AttributeError("Key not existent in configurations")
        return bool(int(settings[key]["bool"]))

    def set_config(self, key, value):
        """
        sets a config setting based on the key

        :param key:
        :param value:
        :return:
        """
        if not self.xml:
            raise AttributeError("XML is not defined or empty.")

        settings = {
            "startup": self.xml.configuration.startup,
            "autoupdate": self.xml.configuration.autoupdate,
            "systemtray": self.xml.configuration.systemtray,
            "cmdgui": self.xml.configuration.cmdgui,
        }
        if key not in settings:
            raise AttributeError("Key not existent in configurations")
        settings[key]["bool"] = int(value)
        self.save_xml()

    def get_proxy_key(self, key):
        """
        gets a proxy key

        :param key:
        :return:
        """
        path = self.xml.configuration.proxy_filter
        if not path.has_attr(key):
            if not self.validate_key(u"configuration.proxy_filter", key):
                raise AttributeError("Key not existent in configurations")
        return path[key]

    def set_proxy_key(self, key, value):
        """
        sets a proxy key

        :param key:
        :param value:
        :return:
        """
        path = self.xml.configuration.proxy_filter
        if path.has_attr(key):
            raise AttributeError("Key not existent in configurations")
        path[key] = str(value)
        self.save_xml()

    def get_localization_key(self, key):
        """
        gets a localization key

        :param key:
        :return:
        """
        path = self.xml.configuration.locale
        if not path.has_attr(key):
            if not self.validate_key(u"configuration.locale", key):
                raise AttributeError("Key not existent in configurations")
        return path[key]

    def set_localization_key(self, key, value):
        """
        sets a localization key

        :param key:
        :param value:
        :return:
        """
        path = self.xml.configuration.locale
        if path.has_attr(key):
            raise AttributeError("Key not existent in configurations")
        path[key] = str(value)
        self.save_xml()

    def create_xml(self, validation=False):
        """
        create empty xml if the xml is not existent

        :param validation:
        :return:
        """
        xml_content = """
        <?xml version="1.0"?>
        <configuration xmlns="settings">
            <startup bool="0">
            </startup>
            <autoupdate bool="1">
            </autoupdate>
            <systemtray bool="0">
            </systemtray>
            <cmdgui bool="0">
            </cmdgui>
            <download directory="Manga">
            </download>
            <database path="database.db">
            <proxy_filter uptime="70" connection="5" level="">
            </proxy_filter>
            </download>
            <locale path="../bin/language/" file="locale.txt">
            </locale>
        </configuration>"""
        if validation:
            return BeautifulSoup(xml_content, features="xml")
        self.xml = BeautifulSoup(xml_content, features="xml")
        self.save_xml()

    def save_xml(self):
        """
        save xml function with synchronized block to prevent multiple write processes at the same time

        :return:
        """
        if not self.file_name:
            raise IOError("No XML file defined.")
        else:
            try:
                self.lock.acquire(True)
                f = open(self.file_name, "w")
                f.write(self.xml.prettify(encoding='utf-8'))
                f.close()
            finally:
                self.lock.release()

    def load_xml(self):
        """
        load xml

        :return:
        """
        if not os.path.isfile(self.file_name):
            self.create_xml()
            self.load_xml()
        if os.stat(self.file_name).st_size == 0:
            self.create_xml()
            self.load_xml()
        else:
            xml_content = open(self.file_name, "r").read()
            self.xml = BeautifulSoup(xml_content, features="xml", from_encoding='utf-8')

    # noinspection PyMethodMayBeStatic
    def recover_key(self, path, key):
        """
        for the case that the process is terminated in the writing process
        we have the option to recreate the default settings instead of raising an error

        :param path:
        :param key:
        :return:
        """
        # ToDo:
        # save settings in the database for a better recreation of the settings
        pass

    def validate_key(self, path, attr):
        """
        validates attribute and path compared to the template xml.
        fixes the xml if key is in the template xml or returns False if key is not in the template file

        :param path:
        :param attr:
        :return:
        """
        template_xml = self.create_xml(validation=True)
        validating_xml = self.xml
        for part in path.split("."):
            if not hasattr(template_xml, "find") or not hasattr(validating_xml, "find"):
                break
            template_xml = template_xml.find(part)
            validating_xml = validating_xml.find(part)
        if not template_xml:
            return False
        if not validating_xml:
            # hard to fix path errors, most likely a complete loss of the content
            self.create_xml()
        else:
            key_res = template_xml.has_attr(attr)
            if not key_res:
                return False
            else:
                # self.recover_key(path, attr)
                validating_xml[attr] = template_xml[attr]
                self.save_xml()
        return True


if __name__ == '__main__':
    # test_manga_jp = '盾の勇者の成り上がり'
    # test_manga_kr = '방패 용자의 성공기'
    # test_manga = test_manga_jp
    parser = XMLParser()
    print "Configuration startup: {0:b}".format(parser.get_config("startup"))
    parser.set_config("startup", False)
    print parser.get_database_path()
    print parser.get_localization_key("path")
    print parser.get_localization_key("file")
