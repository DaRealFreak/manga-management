#!/usr/local/bin/python
# coding: utf-8

import StringIO
import fnmatch
import httplib
import imp
import os
import random
import socket
import struct
import sys
import urllib2

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


# noinspection PyShadowingBuiltins
class Session(object):
    """
    session management, load and generate dynamically session modules in the sessions folder
    """

    # absolute path, since relative paths won't work with test cases
    session_dict = {}
    path = os.path.join(os.path.dirname(__file__), "sessions")

    def get_session(self, type):
        """
        creates session based on needed type

        :param type:
        :return:
        """
        socket.setdefaulttimeout(10)
        if not self.session_dict:
            self.load_sessions()

        if type not in self.session_dict:
            err = "'{0:s}' object has no session '{1:s}'".format(self.__class__.__name__, type)
            raise AttributeError(err)
        else:
            return self._create_session(type)

    def load_sessions(self):
        """
        load all sessions dynamically

        :return:
        """
        if self.path not in sys.path:
            sys.path.insert(0, self.path)

        for session in fnmatch.filter(os.listdir(self.path), 'session_*.py'):
            session_name = session.rsplit('.', 1)[0]
            m = imp.load_source(session_name, os.path.join(self.path, session))
            self.session_dict.setdefault(session_name.split("session_")[1], m.instance)

    def _create_session(self, name):
        """
        create the session for the browser object

        :param name:
        :return:
        """
        return self.session_dict[name]()

    @staticmethod
    def get_random_useragent():
        """
        copypasted from here:
        http://teh-1337.studiobebop.net/blog.py?post=204
        don't like the thousand function calls but whatever

        :return:
        """
        return "Mozilla/{0:.1f} (Windows; U; Windows NT 5.1; en-US; rv:{1:.1f}.{2:.1f}) Gecko/{3:d}0{4:d} " \
               "Firefox/{5:.1f}.{6:.1f}".format((random.random() + 5), (random.random() + random.randint(1, 8)),
                                                random.random(), random.randint(2000, 2100),
                                                random.randint(92215, 99999), (random.random() + random.randint(3, 9)),
                                                random.random())

    @staticmethod
    def is_connected(remote_server="www.google.com"):
        """
        checks for internet connection, trying to create a connection

        :param remote_server:
        :return:
        """
        try:
            host = socket.gethostbyname(remote_server)
            _ = socket.create_connection((host, 80), 2)
            return True
        except socket.timeout:
            return False

    @staticmethod
    def get_image_size(url, headers):
        """
        fast image size check only downloading the header parts of the image file(24 bytes)

        :param url:
        :param headers:
        :return:
        """
        try:
            req = urllib2.Request(url, headers=headers)
            data = urllib2.urlopen(req).read(24)
        except (urllib2.URLError, urllib2.HTTPError, socket.timeout, IOError, httplib.HTTPException, socket.error):
            return "", -1, -1
        data = str(data)
        size = len(data)
        height = -1
        width = -1
        content_type = ''

        # handle GIFs
        if (size >= 10) and data[:6] in ('GIF87a', 'GIF89a'):
            # Check to see if content_type is correct
            content_type = 'image/gif'
            w, h = struct.unpack("<HH", data[6:10])
            width = int(w)
            height = int(h)

        # See PNG 2. Edition spec (http://www.w3.org/TR/PNG/)
        # Bytes 0-7 are below, 4-byte chunk length, then 'IHDR'
        # and finally the 4-byte width, height
        elif (size >= 24) and data.startswith('\211PNG\r\n\032\n') and (data[12:16] == 'IHDR'):
            content_type = 'image/png'
            w, h = struct.unpack(">LL", data[16:24])
            width = int(w)
            height = int(h)

        # Maybe this is for an older PNG version.
        elif (size >= 16) and data.startswith('\211PNG\r\n\032\n'):
            # Check to see if we have the right content type
            content_type = 'image/png'
            w, h = struct.unpack(">LL", data[8:16])
            width = int(w)
            height = int(h)

        # handle JPEGs
        elif (size >= 2) and data.startswith('\377\330'):
            content_type = 'image/jpeg'
            jpeg = StringIO.StringIO(data)
            jpeg.read(2)
            b = jpeg.read(1)
            try:
                while b and ord(b) != 0xDA:
                    while ord(b) != 0xFF:
                        b = jpeg.read(1)
                    while ord(b) == 0xFF:
                        b = jpeg.read(1)
                    if 0xC0 <= ord(b) <= 0xC3:
                        jpeg.read(3)
                        h, w = struct.unpack(">HH", jpeg.read(4))
                        break
                    else:
                        jpeg.read(int(struct.unpack(">H", jpeg.read(2))[0]) - 2)
                    b = jpeg.read(1)
                # noinspection PyUnboundLocalVariable
                width = int(w)
                # noinspection PyUnboundLocalVariable
                height = int(h)
            except struct.error:
                pass
            except ValueError:
                pass

        return content_type, width, height


if __name__ == '__main__':
    import time

    test_sessions = ("mechanize", "socket", "urllib", "ghost", "phantomJS")
    for test_session in test_sessions:
        start = time.time()
        print "Testing module: {0:s}".format(test_session)
        if Session.is_connected():
            session_object = Session().get_session(test_session)
            src = session_object.open("http://www.google.de/intl/en/policies/privacy/?fg=1").read()
            print "Content length: {0:d}".format(len(src))
            print "Test successful: {0:b}".format("This is important" in src)

            open("test_{0:s}.gif".format(test_session), "wb").write(
                session_object.open("http://i.epvpimg.com/qC6Gb.gif").read()
            )

            print "Test duration: {0:f}".format(time.time() - start)
            print session_object.get_user_agent()
            # print session_object.get_session().open("https://www.google.de").read()
