#!/usr/local/bin/python
# coding: utf-8

from management import databaseIO
from management import download
from management import modules
from management import proxy
from management import session
from management import threads
from management import title_search
from management import update
from management import xml_parser
from management.main import MangaManagement

__version__ = '0.0.3'
__name__ = 'Manga Management'
__author__ = 'DaRealFreak <dasbaumchen@web.de>'
__all__ = ['MangaManagement', 'databaseIO', 'download', 'modules', 'proxy', 'session', 'threads', 'title_search',
           'update', 'xml_parser']
