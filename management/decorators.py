#!/usr/local/bin/python
# coding: utf-8

import functools
import warnings
from threading import Thread

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


class RunAsynch(object):
    def __init__(self, name=""):
        """
        initializing function

        :param name:
        :return:
        """
        self.name = name

    def __call__(self, func):
        """
        define our decorator object and return the wrapper instance

        :param func:
        :return:
        """
        decorator_object = self

        def wrapper(*args, **kwargs):
            """
            real wrapper function, start our threaded function

            :param args:
            :param kwargs:
            :return:
            """
            if decorator_object.name:
                func_hl = Thread(target=func, name=decorator_object.name, args=args, kwargs=kwargs)
            else:
                func_hl = Thread(target=func, args=args, kwargs=kwargs)
            func_hl.start()
            return func_hl

        return wrapper


def deprecated(func):
    """
    This is a decorator which can be used to mark functions
    as deprecated. It will result in a warning being emitted
    when the function is used.

    :param func:
    :return:
    """

    @functools.wraps(func)
    def deprecated_notification(*args, **kwargs):
        """
        using warning module to show the warning of the deprecated function

        :param args:
        :param kwargs:
        :return:
        """
        warnings.warn_explicit(
            "Call to deprecated function {}.".format(func.__name__),
            category=DeprecationWarning,
            filename=func.func_code.co_filename,
            lineno=func.func_code.co_firstlineno + 1
        )
        return func(*args, **kwargs)

    return deprecated_notification
