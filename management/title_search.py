#!/usr/local/bin/python
# coding: utf-8

import fnmatch
import os
import sys
from collections import Counter

import modules

__author__ = 'DaRealFreak <dasbaumchen@web.de>'


class LanguageTitleSearch(object):
    """
    searching external sites for alternate title names
    for multilinguality
    """

    modules = dict()
    path = os.path.join(os.path.dirname(__file__), "titles")

    def __init__(self):
        """
        initializing function

        :return:
        """
        self.load_modules()

    def load_modules(self):
        """
        load all modules dynamically

        :return:
        """
        if self.path not in sys.path:
            sys.path.insert(0, self.path)

        available_modules = fnmatch.filter(os.listdir(self.path), '*.py')
        if '__init__.py' in available_modules:
            available_modules.remove('__init__.py')

        for module in available_modules:
            m = __import__(u'{0:s}'.format(module.lower().rsplit('.', 1)[0]))
            if 'instance' in dir(m):
                self.modules[module.lower().rsplit('.', 1)[0]] = m.instance()

    def search(self, title):
        """
        main function for extracting alternate titles

        :param title:
        :return:
        """
        matches = Counter()
        for module in self.modules:
            matches = Counter(matches + Counter(self.modules[module].search_titles(title)))
        return dict(matches)


class Search(object):
    """
    search class, including functions based around searching and updating
    """

    modules = {}

    def __init__(self):
        """
        initializing function

        :return:
        """
        if not self.modules:
            module_manager = modules.ModuleManager()
            self.modules = module_manager.get_all_modules()

    def search_modules(self, title):
        """
        search through the available modules

        :param title:
        :return:
        """
        results = []
        for key in self.modules.keys():
            res = self.modules[key].search(title)
            if not res or res == [''] * 7:
                continue
            # title, url, chapter_count, latest_chapter, language, module, active = res
            results.append((res, self.modules[key]))
        return results


if __name__ == '__main__':
    test_manga = "Doulou Dalu"
    test_manga_2 = "Tate no Yuusha no Nariagari"
    test_manga_3 = "Gunota ga Mahou Sekai ni Tensei Shitara, Gendai Heiki de Guntai Harem o Tsukucchaimashita"
    test_manga_4 = 'Ore ga Ojou-sama Gakkou ni "Shomin Sample" Toshite Rachirareta Ken'
    # s = Search()
    # print s.search_modules("Doulou Dalu")
    ts = LanguageTitleSearch()
    print ts.search(test_manga_3)
