#!/usr/bin/env python
from __future__ import print_function

import sys
from codecs import open as codecs_open

from setuptools import setup, find_packages

install_requires = [
    'BeautifulSoup',
    'lxml',
    'numpy',
    'mechanize',
]

if sys.platform in ('linux', 'linux2'):
    # Linux
    install_requires.append("wx")
elif sys.platform == "win32":
    # Windows
    install_requires.append("pywin32")
elif sys.platform == "darwin":
    # Mac OS X
    install_requires.append("rumps")

# Get the long description from the relevant file
with codecs_open('README.md', encoding='utf-8') as f:
    long_description = f.read()

setup(
        name='management',
        version='0.0.1',
        url='https://bitbucket.org/DaRealFreak/manga-management',
        license='MIT',
        author='DaRealFreak',
        install_requires=install_requires,
        extras_require={
            'sessions_optional': [
                'PyQt4',
                'selenium',
                'ghost.py',
            ]
        },
        author_email='dasbaumchen@web.com',
        description='A python based management for mangas',
        long_description=long_description,
        packages=find_packages(),
        package_data={
            # include localization files and the phantomJS binary
            '': ['*.txt', '*.exe'],
        },
        include_package_data=True,
        platforms='any',
        classifiers=[
            'Programming Language :: Python',
            'Natural Language :: English',
            'Intended Audience :: Developers, Readers',
            'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
            'Operating System :: OS Independent',
        ],
)
