# Manga Management

Manga Management is a python based management for different manga.
It keeps the saved manga secured in a local database file and checks in set intervals for updates on your selected manga site.
It's easy to extend with your own modules as it provides dynamic loading of your modules and patterns for the site.
You can also add your own session modules and title search modules if your site requires it.

### Version
0.0.1

### Dependencies

Manga Management uses the external binary of the open source project phantomJS to work properly on the usage of phantomJS with windows(stored in the bin folder).
The phantomJS module is disabled in MacOS and Linux.
As it provides a large quantity of session modules we have a lot of optional dependencies which you are free to use for further improvement and possibilities.

Required:

* [BeautifulSoup] - html/xml file parser
* [lxml] - xml parser, used by BeautifulSoup for xml parsing
* [numpy] - fundamental package for scientific computing with Python
* [mechanize] - simlified session handling

Optional:

* [PyQt4] - cross-platform python user interface
* [selenium] - API used for PhantomJS embedding
* [ghost] - PyQt4 based JavaScript able webkit
* [PhantomJs] - headless WebKit scriptable with a JavaScript API

### Usage

To track and update the saved manga, the management is mostly active in the background, extracting available sale informations from the responsible sites
A command line interface is responsible for the interaction with the user.

```sh
$ python management
```

### Development

Want to contribute? Great!
The session and manga modules are loaded dynamically, so every user can contribute without the need to change the other code too.

I'm always glad hearing about bugs or pull requests.

### Todos

 - adding user interface
 - add new modules(marumaru.in, comico.jp, mangahop.com)

License
----

GPL


   [PhantomJs]: <https://github.com/ariya/phantomjs/>
   [selenium]: <https://pypi.python.org/pypi/selenium>
   [PyQt4]: <https://www.riverbankcomputing.com/software/pyqt/intro>
   [BeautifulSoup]: <http://www.crummy.com/software/BeautifulSoup/>
   [ghost]: <https://github.com/jeanphix/Ghost.py>
   [lxml]: <http://lxml.de/>
   [mechanize]: <https://github.com/jjlee/mechanize>
   [numpy]: <http://sourceforge.net/projects/numpy/files/>